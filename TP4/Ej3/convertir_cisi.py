# -*- coding: utf-8 -*-

import re
import codecs
from collections import OrderedDict


def procesar_corpus(path_corpus):
    with codecs.open(path_corpus, mode="r", encoding="utf-8") as corpus:
        texto_corpus = corpus.read()
        lista_docs = re.findall("\n\.W([\s|\S]*?)\n\.X", texto_corpus)
    return lista_docs


def cargar_stopwords(path_stopwords):
    with codecs.open(path_stopwords, mode="r", encoding="utf-8") as stopwords:
        texto_stopwords = stopwords.read()
        texto_stopwords = texto_stopwords.lower()
        texto_stopwords = re.sub("[^a-z]", " ", texto_stopwords)
        lista_stopwords = texto_stopwords.split()
    return lista_stopwords


def procesar_queries(path_queries, lista_stopwords=None):
    with codecs.open(path_queries, mode="r", encoding="utf-8") as queries:
        texto_queries = queries.read()
        lista_queries = re.findall("\n\.W([\s|\S]*?)(?:\n\.I|\n\.B)", texto_queries)
        lista_queries_sin_duplicados = []
    for indice in xrange(len(lista_queries)):
        lista_queries[indice] = lista_queries[indice].lower()
        lista_queries[indice] = re.sub("[^a-z]", " ", lista_queries[indice])
        lista_queries[indice] = lista_queries[indice].split()
        lista_queries[indice] = [t for t in lista_queries[indice] if len(t) > 3]
        if lista_stopwords is not None:
            lista_queries[indice] = [t for t in lista_queries[indice] if t not in lista_stopwords]
        sin_duplicados = list(OrderedDict.fromkeys(lista_queries[indice]))
        lista_queries[indice] = " ".join(lista_queries[indice])
        lista_queries_sin_duplicados.append(" ".join(sin_duplicados))
    return lista_queries, lista_queries_sin_duplicados


def procesar_relevantes(path_relevantes):
    with codecs.open(path_relevantes, mode="r", encoding="utf-8") as relevantes:
        lista_relevantes = []
        for linea in relevantes:
            lista_items = linea.split()
            lista_items[1], lista_items[2] = lista_items[2], lista_items[1]
            lista_items[3] = "1"
            texto_items = " ".join(lista_items)
            lista_relevantes.append(texto_items)
    return lista_relevantes


def guardar_corpus(path_corpus_trec, lista_docs):
    with codecs.open(path_corpus_trec, mode="w", encoding="utf-8") as corpus_trec:
        for indice, documento in enumerate(lista_docs):
            corpus_trec.write("<DOC>\n<DOCNO>" + str(indice+1) + "</DOCNO>")
            corpus_trec.write(documento + "\n</DOC>\n")


def guardar_queries(path_queries_trec, lista_queries):
    with codecs.open(path_queries_trec, mode="w", encoding="utf-8") as queries_trec:
        for indice, query in enumerate(lista_queries):
            queries_trec.write("<TOP>\n<NUM>" + str(indice+1) + "<NUM>\n")
            queries_trec.write("<TITLE>" + query + "\n</TOP>\n")


def guardar_relevantes(path_relevantes_trec, lista_relevantes):
    with codecs.open(path_relevantes_trec, mode="w", encoding="utf-8") as relevantes_trec:
        for linea in lista_relevantes:
            relevantes_trec.write(linea + "\n")


def main():
    path_corpus = "cisi/CISI.ALL"
    path_queries = "cisi/CISI.QRY"
    path_relevantes = "cisi/CISI.REL"
    path_stopwords = "stopword-list.txt"
    path_corpus_trec = "documentos.trec"
    path_queries_trec = "queries.txt"
    path_queries_sin_duplicados_trec = "queries_sin_duplicados.txt"
    path_relevantes_trec = "relevantes.txt"
    lista_docs = procesar_corpus(path_corpus)
    lista_stopwords = cargar_stopwords(path_stopwords)
    lista_queries, lista_queries_sin_duplicados = procesar_queries(path_queries, lista_stopwords)
    lista_relevantes = procesar_relevantes(path_relevantes)
    guardar_corpus(path_corpus_trec, lista_docs)
    guardar_queries(path_queries_trec, lista_queries)
    guardar_queries(path_queries_sin_duplicados_trec, lista_queries_sin_duplicados)
    guardar_relevantes(path_relevantes_trec, lista_relevantes)

if __name__ == "__main__":
    main()
