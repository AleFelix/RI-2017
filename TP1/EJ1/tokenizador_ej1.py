# -*- coding: utf-8 -*-

import re
import sys
import codecs
from os import listdir
from collections import Counter
from os.path import isdir, isfile, join

sacar_palabras_vacias = False
archivo_vacias = ""
archivo_terminos = "terminos.txt"
archivo_estadisticas = "estadisticas.txt"
archivo_frecuentes = "frecuentes.txt"
longitud_maxima = 15
longitud_minima = 3


class RecopiladorDeEstadisticas(object):
    def __init__(self, nombre_archivo_estadisticas, nombre_archivo_frecuentes):
        self.nombre_archivo_estadisticas = nombre_archivo_estadisticas
        self.nombre_archivo_frecuentes = nombre_archivo_frecuentes
        self.documentos_procesados = 0
        self.tokens_extraidos = 0
        self.terminos_extraidos = 0
        self.promedio_tokens_documentos = 0
        self.promedio_terminos_documentos = 0
        self.largo_promedio_terminos = 0
        self.documentos = {}
        self.documento_mas_corto = {}
        self.documento_mas_largo = {}
        self.cantidad_terminos_unicos = 0
        self.terminos_mas_frecuentes = {}
        self.terminos_menos_frecuentes = {}
        self.lista_mas_frecuentes = []
        self.lista_menos_frecuentes = []

    def contar_documento(self, nombre_documento):
        if self.documentos[nombre_documento]["cant_tokens"] > 0:
            self.documentos_procesados += 1

    def obtener_documentos_extremos(self):
        long_documento_largo = max(doc["cant_tokens"] for doc in self.documentos.values())
        long_documento_corto = min(doc["cant_tokens"] for doc in self.documentos.values())
        for nombre_doc, freq_doc in self.documentos.items():
            if freq_doc["cant_tokens"] == long_documento_largo:
                self.documento_mas_largo[nombre_doc] = freq_doc
            if freq_doc["cant_tokens"] == long_documento_corto:
                self.documento_mas_corto[nombre_doc] = freq_doc

    def contar_terminos_unicos(self, terminos):
        for termino, frecuencia in terminos.items():
            if frecuencia["CF"] == 1:
                self.cantidad_terminos_unicos += 1

    def calcular_promedio_tokens_terminos_documento(self):
        cantidad_documentos = len(self.documentos.keys())
        totales = Counter()
        for valores in self.documentos.values():
            totales.update(valores)
        self.promedio_tokens_documentos = round(totales["cant_tokens"] / float(cantidad_documentos), 2)
        self.promedio_terminos_documentos = round(totales["cant_terminos"] / float(cantidad_documentos), 2)

    def calcular_largo_promedio_terminos(self, terminos):
        suma_longitud = 0
        cantidad_terminos = 0
        for termino in terminos.keys():
            suma_longitud += len(termino)
            cantidad_terminos += 1
        self.largo_promedio_terminos = round(suma_longitud / float(cantidad_terminos), 2)

    def obtener_terminos_mas_menos_frecuentes(self, terminos):
        self.lista_menos_frecuentes = sorted(terminos.keys(), key=lambda x: (terminos[x]["CF"]))[:10]
        self.lista_mas_frecuentes = sorted(terminos.keys(), key=lambda x: (terminos[x]["CF"]), reverse=True)[:10]
        for termino in self.lista_menos_frecuentes:
            self.terminos_menos_frecuentes[termino] = terminos[termino]["CF"]
        for termino in self.lista_mas_frecuentes:
            self.terminos_mas_frecuentes[termino] = terminos[termino]["CF"]

    def mostrar_estadisticas(self):
        print u"ESTADÍSTICAS DE LOS DOCUMENTOS PROCESADOS"
        print "--------------------------------------------------------------------------------"
        print "Numero de documentos procesados: " + str(self.documentos_procesados)
        print "--------------------------------------------------------------------------------"
        print "Cantidad de tokens encontrados: " + str(self.tokens_extraidos)
        print u"Cantidad de términos encontrados: " + str(self.terminos_extraidos)
        print "--------------------------------------------------------------------------------"
        print "Promedio de tokens por documento: " + str(self.promedio_tokens_documentos)
        print u"Promedio de términos por documento: " + str(self.promedio_terminos_documentos)
        print u"Longitud promedio de un término: " + str(self.largo_promedio_terminos)
        print "--------------------------------------------------------------------------------"
        doc_mas_largo = self.documento_mas_largo.keys()[0]
        doc_mas_corto = self.documento_mas_corto.keys()[0]
        print "Documento mas largo: " + doc_mas_largo
        print "Cantidad de tokens: " + str(self.documento_mas_largo[doc_mas_largo]["cant_tokens"])
        print u"Cantidad de términos: " + str(self.documento_mas_largo[doc_mas_largo]["cant_terminos"])
        print "--------------------------------------------------------------------------------"
        print "Documento mas corto: " + doc_mas_corto
        print "Cantidad de tokens: " + str(self.documento_mas_corto[doc_mas_corto]["cant_tokens"])
        print u"Cantidad de términos: " + str(self.documento_mas_corto[doc_mas_corto]["cant_terminos"])
        print "--------------------------------------------------------------------------------"
        print u"Cantidad de términos únicos: " + str(self.cantidad_terminos_unicos)
        print "--------------------------------------------------------------------------------"

    def mostrar_terminos_mas_menos_frecuentes(self):
        print u"10 TÉRMINOS MAS FRECUENTES"
        print "--------------------------------------------------------------------------------"
        for termino in self.lista_mas_frecuentes:
            print termino + ": " + str(self.terminos_mas_frecuentes[termino])
        print "--------------------------------------------------------------------------------"
        print u"10 TÉRMINOS MENOS FRECUENTES"
        print "--------------------------------------------------------------------------------"
        for termino in self.lista_menos_frecuentes:
            print termino + ": " + str(self.terminos_menos_frecuentes[termino])
        print "--------------------------------------------------------------------------------"

    def guardar_estadisticas(self):
        with codecs.open(self.nombre_archivo_estadisticas, mode="w", encoding="utf-8") as archivo_est:
            archivo_est.write(u"ESTADÍSTICAS DE LOS DOCUMENTOS PROCESADOS\n")
            archivo_est.write("--------------------------------------------------------------------------------\n")
            archivo_est.write("Numero de documentos procesados: " + str(self.documentos_procesados) + "\n")
            archivo_est.write("--------------------------------------------------------------------------------\n")
            archivo_est.write("Cantidad de tokens encontrados: " + str(self.tokens_extraidos) + "\n")
            archivo_est.write(u"Cantidad de términos encontrados: " + str(self.terminos_extraidos) + "\n")
            archivo_est.write("--------------------------------------------------------------------------------\n")
            archivo_est.write("Promedio de tokens por documento: " + str(self.promedio_tokens_documentos) + "\n")
            archivo_est.write(u"Promedio de términos por documento: " + str(self.promedio_terminos_documentos) + "\n")
            archivo_est.write(u"Longitud promedio de un término: " + str(self.largo_promedio_terminos) + "\n")
            archivo_est.write("--------------------------------------------------------------------------------\n")
            doc_mas_largo = self.documento_mas_largo.keys()[0]
            doc_mas_corto = self.documento_mas_corto.keys()[0]
            archivo_est.write("Documento mas largo: " + doc_mas_largo + "\n")
            archivo_est.write("Cantidad de tokens: " + str(self.documento_mas_largo[doc_mas_largo]["cant_tokens"]) +
                              "\n")
            archivo_est.write(u"Cantidad de términos: " + str(self.documento_mas_largo[doc_mas_largo]["cant_terminos"])
                              + "\n")
            archivo_est.write("--------------------------------------------------------------------------------\n")
            archivo_est.write("Documento mas corto: " + doc_mas_corto + "\n")
            archivo_est.write("Cantidad de tokens: " + str(self.documento_mas_corto[doc_mas_corto]["cant_tokens"]) +
                              "\n")
            archivo_est.write(u"Cantidad de términos: " + str(self.documento_mas_corto[doc_mas_corto]["cant_terminos"])
                              + "\n")
            archivo_est.write("--------------------------------------------------------------------------------\n")
            archivo_est.write(u"Cantidad de términos únicos: " + str(self.cantidad_terminos_unicos) + "\n")
            archivo_est.write("--------------------------------------------------------------------------------\n")

    def guardar_terminos_mas_menos_frecuentes(self):
        with codecs.open(self.nombre_archivo_frecuentes, mode="w", encoding="utf-8") as archivo_frec:
            archivo_frec.write(u"10 TÉRMINOS MAS FRECUENTES\n")
            archivo_frec.write("--------------------------------------------------------------------------------\n")
            for termino in self.lista_mas_frecuentes:
                archivo_frec.write(termino + ": " + str(self.terminos_mas_frecuentes[termino]) + "\n")
            archivo_frec.write("--------------------------------------------------------------------------------\n")
            archivo_frec.write(u"10 TÉRMINOS MENOS FRECUENTES\n")
            archivo_frec.write("--------------------------------------------------------------------------------\n")
            for termino in self.lista_menos_frecuentes:
                archivo_frec.write(termino + ": " + str(self.terminos_menos_frecuentes[termino]) + "\n")
            archivo_frec.write("--------------------------------------------------------------------------------\n")


class Tokenizador(object):
    def __init__(self, path_directorio, nombre_archivo_terminos, sacar_vacias, nombre_archivo_vacias,
                 min_longitud_termino, max_longitud_termino, nombre_archivo_estadisticas, nombre_archivo_frecuentes):
        self.recopilador_estadisticas = RecopiladorDeEstadisticas(nombre_archivo_estadisticas,
                                                                  nombre_archivo_frecuentes)
        self.path_directorio = path_directorio
        self.nombre_archivo_terminos = nombre_archivo_terminos
        self.sacar_vacias = sacar_vacias
        self.nombre_archivo_vacias = nombre_archivo_vacias
        self.lista_vacias = []
        self.min_longitud_termino = min_longitud_termino
        self.max_longitud_termino = max_longitud_termino
        self.terminos = {}
        self.id_doc_actual = 0
        self.tokens = []

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    def analizar_directorio(self):
        if not isdir(self.path_directorio):
            print u"ERROR: Debe ingresar un directorio válido"
            return False
        if self.sacar_vacias and not isfile(self.nombre_archivo_vacias):
            print u"ERROR: Debe ingresar un archivo de palabras vacias válido"
            return False
        if self.sacar_vacias and not self.cargar_lista_vacias():
            print u"ERROR: No se pudo cargar el archivo de palabras vacias"
            return False
        lista_archivos = listdir(self.path_directorio)
        if not lista_archivos:
            print u"ERROR: El directorio a analizar esta vacío"
            return False
        long_lista_archivos = len(lista_archivos)
        for indice_archivo, nombre_archivo in enumerate(lista_archivos):
            porcentaje = ((indice_archivo + 1) / float(long_lista_archivos)) * 100
            sys.stdout.write(u"\rANÁLISIS DE DOCUMENTOS EN PROGRESO: %d%% COMPLETADO" % porcentaje)
            self.analizar_archivo(nombre_archivo)
        print u"\nANÁLISIS DE DOCUMENTOS FINALIZADO"
        self.recopilador_estadisticas.calcular_promedio_tokens_terminos_documento()
        self.recopilador_estadisticas.calcular_largo_promedio_terminos(self.terminos)
        self.recopilador_estadisticas.obtener_documentos_extremos()
        self.recopilador_estadisticas.contar_terminos_unicos(self.terminos)
        self.recopilador_estadisticas.obtener_terminos_mas_menos_frecuentes(self.terminos)
        return True

    def cargar_lista_vacias(self):
        try:
            with codecs.open(self.nombre_archivo_vacias, mode="r", encoding="utf-8") as vacias:
                for linea in vacias:
                    linea = Tokenizador.translate(linea)
                    palabras = linea.strip().split()
                    for palabra in palabras:
                        if palabra not in self.lista_vacias:
                            self.lista_vacias.append(palabra)
            return True
        except ValueError:
            print u"Error en la codificación del archivo de palabras vacias"
            return False

    def analizar_archivo(self, nombre_archivo):
        path_archivo = join(self.path_directorio, nombre_archivo)
        if isfile(path_archivo):
            with codecs.open(path_archivo, mode="r", encoding="utf-8", errors='ignore') as archivo:
                self.id_doc_actual += 1
                self.recopilador_estadisticas.documentos[nombre_archivo] = {
                    "cant_tokens": 0,
                    "cant_terminos": 0
                }
                texto_archivo = archivo.read()
                texto_archivo = self.eliminar_ruido(texto_archivo)
                lista_tokens = self.tokenizar(texto_archivo)
                if self.sacar_vacias:
                    lista_tokens = self.sacar_palabras_vacias(lista_tokens, self.lista_vacias)
                lista_terminos = self.obtener_terminos(lista_tokens)
                self.procesar_terminos(lista_terminos)
                self.recopilador_estadisticas.documentos[nombre_archivo]["cant_tokens"] += len(lista_tokens)
                self.recopilador_estadisticas.documentos[nombre_archivo]["cant_terminos"] += len(lista_terminos)
                self.recopilador_estadisticas.contar_documento(nombre_archivo)

    @staticmethod
    def eliminar_ruido(string):
        cantidad_subs = 1
        while cantidad_subs > 0:
            string, cantidad_subs = re.subn("<[^<>]*>", " ", string)
        cantidad_subs = 1
        while cantidad_subs > 0:
            string, cantidad_subs = re.subn("\{[^{\}]*\}", " ", string)
        return string

    @staticmethod
    def tokenizar(string):
        string = string.lower()
        string = Tokenizador.translate(string)
        string = re.sub(u"[^a-zñ]|_", " ", string)
        lista_tokens = string.strip().split()
        return lista_tokens

    @staticmethod
    def sacar_palabras_vacias(lista_tokens, lista_vacias):
        for palabra in lista_tokens:
            if palabra in lista_vacias:
                lista_tokens.remove(palabra)
        return lista_tokens

    def obtener_terminos(self, lista_tokens):
        lista_terminos = []
        for token in lista_tokens:
            self.recopilador_estadisticas.tokens_extraidos += 1
            if token not in self.tokens:
                self.tokens.append(token)
            if self.min_longitud_termino < len(token) < self.max_longitud_termino:
                lista_terminos.append(token)
                self.recopilador_estadisticas.terminos_extraidos += 1
        return lista_terminos

    def procesar_terminos(self, lista_terminos):
        for termino in lista_terminos:
            if termino not in self.terminos:
                self.terminos[termino] = {}
                self.terminos[termino]["CF"] = 1
                self.terminos[termino]["DF"] = 1
                self.terminos[termino]["Docs"] = [self.id_doc_actual]
            else:
                self.terminos[termino]["CF"] += 1
                if self.id_doc_actual not in self.terminos[termino]["Docs"]:
                    self.terminos[termino]["DF"] += 1
                    self.terminos[termino]["Docs"].append(self.id_doc_actual)

    def mostrar_terminos(self):
        print u"LISTA DE TÉRMINOS ENCONTRADOS"
        for termino in sorted(self.terminos.keys()):
            print "--------------------------------------------------------------------------------"
            print termino
            print "CF: " + str(self.terminos[termino]["CF"])
            print "DF: " + str(self.terminos[termino]["DF"])
        print "--------------------------------------------------------------------------------"

    def guardar_terminos(self):
        with codecs.open(self.nombre_archivo_terminos, mode="w", encoding="utf-8") as archivo_term:
            archivo_term.write(u"LISTA DE TÉRMINOS ENCONTRADOS\n")
            for termino in sorted(self.terminos.keys()):
                archivo_term.write("--------------------------------------------------------------------------------\n")
                archivo_term.write(termino + "\n")
                archivo_term.write("CF: " + str(self.terminos[termino]["CF"]) + "\n")
                archivo_term.write("DF: " + str(self.terminos[termino]["DF"]) + "\n")
            archivo_term.write("--------------------------------------------------------------------------------\n")

if __name__ == "__main__":
    if "-h" in sys.argv:
        print u"MODO DE USO: tokenizador_ej1.py <path_directorio_archivos> [(-v <nombre_archivo_palabras_vacias>)]"
        sys.exit(0)
    if len(sys.argv) < 2:
        print u"ERROR: Debe ingresar al menos el directorio con los archivos a analizar"
        sys.exit(1)
    if "-v" in sys.argv:
        if sys.argv.index("-v") + 1 == len(sys.argv):
            print u"ERROR: Debe ingresar el nombre del archivo con palabras vacias"
            sys.exit(1)
        else:
            sacar_palabras_vacias = True
            archivo_vacias = sys.argv[sys.argv.index("-v") + 1]
    tokenizador = Tokenizador(sys.argv[1], archivo_terminos, sacar_palabras_vacias, archivo_vacias, longitud_minima,
                              longitud_maxima, archivo_estadisticas, archivo_frecuentes)
    if tokenizador.analizar_directorio():
        # tokenizador.mostrar_terminos()
        tokenizador.recopilador_estadisticas.mostrar_estadisticas()
        # tokenizador.recopilador_estadisticas.mostrar_terminos_mas_menos_frecuentes()
        tokenizador.guardar_terminos()
        tokenizador.recopilador_estadisticas.guardar_estadisticas()
        tokenizador.recopilador_estadisticas.guardar_terminos_mas_menos_frecuentes()
        sys.exit(0)
    else:
        sys.exit(1)
