# -*- coding: utf-8 -*-

import re
import codecs
from os import listdir
from os.path import isdir, isfile, join


class Tokenizador(object):
    def __init__(self, path_directorio, nombre_archivo_terminos, sacar_vacias, nombre_archivo_vacias,
                 min_longitud_termino, max_longitud_termino):

        self.path_directorio = path_directorio
        self.nombre_archivo_terminos = nombre_archivo_terminos
        self.sacar_vacias = sacar_vacias
        self.nombre_archivo_vacias = nombre_archivo_vacias
        self.lista_vacias = []
        self.min_longitud_termino = min_longitud_termino
        self.max_longitud_termino = max_longitud_termino
        self.terminos = {}
        self.id_doc_actual = 0
        self.tokens = []
        self.terminos_procesados_unicos = {
            "procesados": 0,
            "unicos": 0
        }
        self.lista_terminos_procesados_unicos = []

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    def analizar_directorio(self):
        if not isdir(self.path_directorio):
            print u"ERROR: Debe ingresar un directorio válido"
            return False
        if self.sacar_vacias and not isfile(self.nombre_archivo_vacias):
            print u"ERROR: Debe ingresar un archivo de palabras vacias válido"
            return False
        if self.sacar_vacias and not self.cargar_lista_vacias():
            print u"ERROR: No se pudo cargar el archivo de palabras vacias"
            return False
        lista_archivos = listdir(self.path_directorio)
        if not lista_archivos:
            print u"ERROR: El directorio a analizar esta vacío"
            return False
        print u"ANÁLISIS DE DOCUMENTOS EN PROGRESO"
        for indice_archivo, nombre_archivo in enumerate(lista_archivos):
            self.analizar_archivo(nombre_archivo)
        print u"ANÁLISIS DE DOCUMENTOS FINALIZADO"
        return True

    def cargar_lista_vacias(self):
        try:
            with codecs.open(self.nombre_archivo_vacias, mode="r", encoding="utf-8") as vacias:
                for linea in vacias:
                    linea = Tokenizador.translate(linea)
                    palabras = linea.strip().split()
                    for palabra in palabras:
                        if palabra not in self.lista_vacias:
                            self.lista_vacias.append(palabra)
            return True
        except ValueError:
            print u"Error en la codificación del archivo de palabras vacias"
            return False

    def analizar_archivo(self, nombre_archivo):
        path_archivo = join(self.path_directorio, nombre_archivo)
        if isfile(path_archivo):
            with codecs.open(path_archivo, mode="r", encoding="utf-8", errors='ignore') as archivo:
                self.id_doc_actual += 1
                texto_archivo = archivo.read()
                lista_tokens = self.tokenizar(texto_archivo)
                if self.sacar_vacias:
                    lista_tokens = self.sacar_palabras_vacias(lista_tokens, self.lista_vacias)
                lista_terminos = self.obtener_terminos(lista_tokens)
                self.procesar_terminos(lista_terminos)

    @staticmethod
    def tokenizar(string):
        string = string.lower()
        string = Tokenizador.translate(string)
        string = re.sub(u"[^a-zñ]|_", " ", string)
        lista_tokens = string.strip().split()
        return lista_tokens

    @staticmethod
    def sacar_palabras_vacias(lista_tokens, lista_vacias):
        for palabra in lista_tokens:
            if palabra in lista_vacias:
                lista_tokens.remove(palabra)
        return lista_tokens

    def obtener_terminos(self, lista_tokens):
        lista_terminos = []
        for token in lista_tokens:
            if token not in self.tokens:
                self.tokens.append(token)
            if self.min_longitud_termino < len(token) < self.max_longitud_termino:
                lista_terminos.append(token)
        return lista_terminos

    def procesar_terminos(self, lista_terminos):
        for termino in lista_terminos:
            self.terminos_procesados_unicos["procesados"] += 1
            if termino not in self.terminos:
                self.terminos_procesados_unicos["unicos"] += 1
                self.terminos[termino] = {}
                self.terminos[termino]["CF"] = 1
                self.terminos[termino]["DF"] = 1
                self.terminos[termino]["Docs"] = [self.id_doc_actual]
            else:
                self.terminos[termino]["CF"] += 1
                if self.id_doc_actual not in self.terminos[termino]["Docs"]:
                    self.terminos[termino]["DF"] += 1
                    self.terminos[termino]["Docs"].append(self.id_doc_actual)
            self.lista_terminos_procesados_unicos.append((self.terminos_procesados_unicos["procesados"],
                                                          self.terminos_procesados_unicos["unicos"]))
