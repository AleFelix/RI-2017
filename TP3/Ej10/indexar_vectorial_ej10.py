# -*- coding: utf-8 -*-

import sys
import codecs
from math import sqrt
from os import makedirs
from os.path import join, isdir
from struct import Struct


class ParseadorPostings(object):
    def __init__(self, path_postings):
        self.path_postings = path_postings
        self.vocabulario = {}
        self.ids_docs_corpus = set()

    def cargar_vocabulario(self):
        with codecs.open(self.path_postings, mode="r", encoding="utf-8") as file_postings:
            for linea in file_postings:
                termino, df_termino, ids_docs = linea.split(":")
                ids_docs = ids_docs.strip("\n").strip(",").split(",")
                ids_docs = map(int, ids_docs)
                self.vocabulario[termino] = {}
                for id_doc in ids_docs:
                    self.vocabulario[termino][id_doc] = 1
                self.ids_docs_corpus.update(ids_docs)


class Indexador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FORMATO_PESO = "f"
    FORMATO_IDF = "f"
    FORMATO_NORMA = "I f"
    FILE_TERMINOS = "terminos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    FILE_IDFS = "idfs.bin"
    FILE_NORMAS = "normas.bin"
    FILE_SKIPS = "skips.bin"
    INTERVALO_SKIP = 5

    def __init__(self, vocabulario, ids_docs, dir_indices):
        self.crear_directorio(dir_indices)
        self.vocabulario = vocabulario
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_postings = join(dir_indices, self.FILE_POSTINGS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_idfs = join(dir_indices, self.FILE_IDFS)
        self.path_normas = join(dir_indices, self.FILE_NORMAS)
        self.path_skips = join(dir_indices, self.FILE_SKIPS)
        self.indice = {}
        self.terminos = sorted([termino for termino in self.vocabulario])
        self.idf_terminos = []
        self.pesos_terminos_por_doc = None
        self.ids_docs = sorted(list(ids_docs))
        self.skips = {}

    @staticmethod
    def crear_directorio(path_dir):
        try:
            makedirs(path_dir)
        except OSError:
            if not isdir(path_dir):
                raise

    def guardar_terminos(self):
        with codecs.open(self.path_terminos, mode="w", encoding="utf-8") as file_terminos:
            for termino in self.terminos:
                file_terminos.write(termino + "\n")

    def guardar_postings(self):
        packer_post = Struct(self.FORMATO_POSTING)
        packer_peso = Struct(self.FORMATO_PESO)
        pos_posting = 0
        self.pesos_terminos_por_doc = {}
        with open(self.path_postings, mode="wb") as file_postings:
            for id_termino, termino in enumerate(self.terminos):
                self.skips[id_termino] = []
                self.indice[id_termino] = pos_posting
                df_termino = len(self.vocabulario[termino])
                file_postings.write(packer_post.pack(df_termino))
                pos_posting += packer_post.size
                id_doc_anterior = 0
                for pos_doc, id_doc in enumerate(sorted(self.vocabulario[termino])):
                    file_postings.write(packer_post.pack(id_doc - id_doc_anterior))
                    id_doc_anterior = id_doc
                    pos_posting += packer_post.size
                    peso_termino = self.calcular_peso(termino, id_termino, id_doc)
                    file_postings.write(packer_peso.pack(peso_termino))
                    pos_posting += packer_peso.size
                    if id_doc in self.pesos_terminos_por_doc:
                        self.pesos_terminos_por_doc[id_doc].append(peso_termino)
                    else:
                        self.pesos_terminos_por_doc[id_doc] = [peso_termino]
                    if pos_doc % self.INTERVALO_SKIP == 0:
                        self.skips[id_termino].append((id_doc, pos_doc))

    def guardar_skips(self):
        packer = Struct(self.FORMATO_POSTING)
        with open(self.path_skips, mode="wb") as file_skips:
            for id_termino, termino in enumerate(self.terminos):
                file_skips.write(packer.pack(id_termino))
                file_skips.write(packer.pack(len(self.skips[id_termino])))
                for id_doc, pos_doc in self.skips[id_termino]:
                    file_skips.write(packer.pack(id_doc))
                    file_skips.write(packer.pack(pos_doc))

    def guardar_indice(self):
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="wb") as file_indice:
            for id_termino in sorted(self.indice):
                file_indice.write(packer.pack(id_termino, self.indice[id_termino]))

    def guardar_idfs(self):
        packer = Struct(self.FORMATO_IDF)
        with open(self.path_idfs, mode="wb") as file_idfs:
            for id_termino, termino in enumerate(self.terminos):
                idf_termino = self.idf_terminos[id_termino]
                file_idfs.write(packer.pack(idf_termino))

    def calcular_peso(self, termino, id_termino, id_doc):
        tf = self.vocabulario[termino][id_doc]
        idf = self.idf_terminos[id_termino]
        return float(tf * idf)

    def calcular_idfs(self):
        tam_corpus = len(self.ids_docs)
        for id_termino, termino in enumerate(self.terminos):
            df_termino = len(self.vocabulario[termino])
            idf_termino = tam_corpus / float(df_termino)
            self.idf_terminos.append(idf_termino)

    def guardar_normas_documentos(self):
        packer = Struct(self.FORMATO_NORMA)
        with open(self.path_normas, mode="wb") as file_normas:
            for id_doc in self.ids_docs:
                norma_doc = sqrt(sum([pow(peso, 2) for peso in self.pesos_terminos_por_doc[id_doc]]))
                file_normas.write(packer.pack(id_doc, norma_doc))


def main(dir_postings, dir_indices):
    print u"Parseando las posting-lists..."
    parseador = ParseadorPostings(dir_postings)
    parseador.cargar_vocabulario()
    print u"Parsing completo, vocabulario cargado..."
    indexador = Indexador(parseador.vocabulario, parseador.ids_docs_corpus, dir_indices)
    print u"Guardando los nombres de los términos..."
    indexador.guardar_terminos()
    print u"Calculando los idfs de los términos..."
    indexador.calcular_idfs()
    print u"Guardando las posting-lists..."
    indexador.guardar_postings()
    print u"Guardando las normas..."
    indexador.guardar_normas_documentos()
    print u"Guardando el indice..."
    indexador.guardar_indice()
    print u"Guardando las skips..."
    indexador.guardar_skips()
    print u"Guardando los idfs..."
    indexador.guardar_idfs()
    print u"Finalizado!"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "ERROR: Debe ingresar el archivo con las posting-lists y el directorio destino de los indices."
        print "MODO DE USO: indexar_vectorial_ej10.py <path_postings> <path_indices>"
    else:
        main(sys.argv[1], sys.argv[2])
