# -*- coding: utf-8 -*-

import re
import sys
import time
import codecs
from struct import Struct
from os.path import join
from math import sqrt
from collections import Counter


class ParseadorQueries(object):
    def __init__(self, path_queries):
        self.path_queries = path_queries
        self.queries = []

    def cargar_queries(self):
        with codecs.open(self.path_queries, mode="r", encoding="utf-8") as file_queries:
            for linea in file_queries:
                terminos = self.tokenizar(linea)
                self.queries.append(terminos)

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    @staticmethod
    def tokenizar(texto):
        texto = texto.lower()
        texto = ParseadorQueries.translate(texto)
        texto = re.sub(u"[^a-zñ0-9]|_", " ", texto)
        return texto.split()


class Buscador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FORMATO_PESO = "f"
    FORMATO_IDF = "f"
    FORMATO_NORMA = "I f"
    FILE_TERMINOS = "terminos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    FILE_IDFS = "idfs.bin"
    FILE_NORMAS = "normas.bin"
    FILE_TIEMPOS = "tiempos.txt"
    TOP_K = 10

    def __init__(self, dir_indices):
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_postings = join(dir_indices, self.FILE_POSTINGS)
        self.path_normas = join(dir_indices, self.FILE_NORMAS)
        self.path_idfs = join(dir_indices, self.FILE_IDFS)
        self.path_tiempos = join(dir_indices, self.FILE_TIEMPOS)
        self.terminos = []
        self.indice = {}
        self.idf_terminos = []
        self.normas_documentos = {}
        self.tiempos_por_longitud = {"TaaT": {}, "DaaT": {}}

    def cargar_terminos(self):  # Archivo de texto con los nombres de los términos separados por saltos de línea
        with codecs.open(self.path_terminos, mode="r", encoding="utf-8") as file_terminos:
            for termino in file_terminos:
                self.terminos.append(termino.strip())

    def cargar_indice(self):  # Formato del indice: [id_término, byte donde empieza la posting del término]
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="rb") as file_indice:
            bytes_indice = file_indice.read(packer.size)
            while bytes_indice:
                elementos_indice = packer.unpack(bytes_indice)
                self.indice[elementos_indice[0]] = elementos_indice[1]
                bytes_indice = file_indice.read(packer.size)

    def cargar_postings_dgaps(self, lista_ids_terminos):  # Formato: id_term, df_term: dgap, peso_term
        packer_post = Struct(self.FORMATO_POSTING)
        packer_peso = Struct(self.FORMATO_PESO)
        with open(self.path_postings, mode="rb") as file_postings:
            docs_por_termino = {}
            for id_termino in lista_ids_terminos:
                docs_por_termino[id_termino] = []
                if id_termino in self.indice:
                    pos_posting = self.indice[id_termino]
                    file_postings.seek(pos_posting)
                    bytes_df = file_postings.read(packer_post.size)
                    df_termino = packer_post.unpack(bytes_df)[0]
                    for i in xrange(df_termino):
                        bytes_dgap = file_postings.read(packer_post.size)
                        dgap = packer_post.unpack(bytes_dgap)[0]
                        bytes_peso = file_postings.read(packer_peso.size)
                        peso_termino = packer_peso.unpack(bytes_peso)[0]
                        docs_por_termino[id_termino].append((dgap, peso_termino))
            return docs_por_termino

    def cargar_idfs(self):  # Formato: [idf_término]
        packer = Struct(self.FORMATO_IDF)
        with open(self.path_idfs, mode="rb") as file_idfs:
            bytes_idf = file_idfs.read(packer.size)
            while bytes_idf:
                idf_termino = packer.unpack(bytes_idf)[0]
                self.idf_terminos.append(idf_termino)
                bytes_idf = file_idfs.read(packer.size)

    def cargar_normas_docs(self):  # Formato: [id_doc, norma_doc]
        packer = Struct(self.FORMATO_NORMA)
        with open(self.path_normas, mode="rb") as file_normas:
            bytes_doc_norma = file_normas.read(packer.size)
            while bytes_doc_norma:
                id_doc, norma_doc = packer.unpack(bytes_doc_norma)
                self.normas_documentos[id_doc] = norma_doc
                bytes_doc_norma = file_normas.read(packer.size)

    def obtener_ids_terminos(self, lista_terminos):
        lista_ids = []
        for termino in lista_terminos:
            if termino in self.terminos:
                lista_ids.append(self.terminos.index(termino))
        return sorted(lista_ids)

    def calcular_peso_termino(self, id_termino, tf_termino):
        return tf_termino * self.idf_terminos[id_termino]

    def cargar_busqueda(self, params):
        if len(params) == 0:
            print u"ERROR: Debe ingresar al menos un término de busqueda"
        else:
            pesos_terminos_query = self.calcular_pesos_query(params)
            self.buscar(pesos_terminos_query)

    def calcular_pesos_query(self, lista_terminos):
        lista_ids = self.obtener_ids_terminos(lista_terminos)
        if not lista_ids:
            return None
        else:
            contador_ids = Counter(lista_ids)
            ids_unicos = contador_ids.keys()
            pesos_terminos_query = {}
            for id_termino in ids_unicos:
                peso_termino = self.calcular_peso_termino(id_termino, contador_ids[id_termino])
                pesos_terminos_query[id_termino] = peso_termino
            return pesos_terminos_query

    @staticmethod
    def calcular_norma(lista_pesos):
        return sqrt(sum(pow(peso, 2) for peso in lista_pesos))

    # noinspection PyTypeChecker
    def buscar(self, pesos_terminos_query):
        if not pesos_terminos_query:
            return None, None
        else:
            len_query = len(pesos_terminos_query)
            if len_query not in self.tiempos_por_longitud["TaaT"]:
                self.tiempos_por_longitud["TaaT"][len_query] = []
                self.tiempos_por_longitud["DaaT"][len_query] = []
            lista_ids_terminos = [id_termino for id_termino in pesos_terminos_query]
            lista_pesos_terminos = [pesos_terminos_query[id_termino] for id_termino in pesos_terminos_query]
            postings_dgaps = self.cargar_postings_dgaps(lista_ids_terminos)
            postings = self.procesar_dgaps(postings_dgaps)
            norma_query = self.calcular_norma(lista_pesos_terminos)
            tiempo_inicio = time.time()
            self.buscar_con_taat(lista_ids_terminos, postings, pesos_terminos_query, norma_query)
            tiempo_taat = time.time() - tiempo_inicio
            tiempo_inicio = time.time()
            self.buscar_con_daat(lista_ids_terminos, postings, pesos_terminos_query, norma_query)
            tiempo_daat = time.time() - tiempo_inicio
            self.tiempos_por_longitud["TaaT"][len_query].append(tiempo_taat)
            self.tiempos_por_longitud["DaaT"][len_query].append(tiempo_daat)
            return tiempo_taat, tiempo_daat

    def buscar_con_taat(self, lista_ids_terminos, postings, pesos_terminos_query, norma_query):
        score_documentos = {}
        for id_termino in lista_ids_terminos:
            for id_doc in postings[id_termino]:
                if id_doc not in score_documentos:
                    score_documentos[id_doc] = 0
                score_documentos[id_doc] += pesos_terminos_query[id_termino] * postings[id_termino][id_doc]
        for id_doc in score_documentos:
            norma_doc = self.normas_documentos[id_doc]
            score_documentos[id_doc] /= float(norma_doc * norma_query)
            score_documentos[id_doc] = round(score_documentos[id_doc], 5)
        return score_documentos

    def buscar_con_daat(self, lista_ids_terminos, postings, pesos_terminos_query, norma_query):
        lista_docs = set()
        for id_termino in lista_ids_terminos:
            lista_docs.update([id_doc for id_doc in postings[id_termino]])
        score_documentos = {}
        for id_doc in sorted(list(lista_docs)):
            score_documentos[id_doc] = 0
            for id_termino in lista_ids_terminos:
                if id_doc in postings[id_termino]:
                    score_documentos[id_doc] += pesos_terminos_query[id_termino] * postings[id_termino][id_doc]
            norma_doc = self.normas_documentos[id_doc]
            score_documentos[id_doc] /= float(norma_doc * norma_query)
            score_documentos[id_doc] = round(score_documentos[id_doc], 5)
        return score_documentos

    @staticmethod
    def procesar_dgaps(postings_dgaps):
        postings = {}
        for id_termino in postings_dgaps:
            postings[id_termino] = {}
            id_doc_anterior = 0
            for dgap, peso in postings_dgaps[id_termino]:
                id_doc = dgap + id_doc_anterior
                postings[id_termino][id_doc] = peso
                id_doc_anterior = id_doc
        return postings

    # noinspection PyTypeChecker
    def guardar_tiempos(self):
        with codecs.open(self.path_tiempos, mode="w", encoding="utf-8") as file_tiempos:
            for tipo_busqueda in self.tiempos_por_longitud:
                file_tiempos.write("------------------------------------------------\n")
                file_tiempos.write(u"Estadísticas de las búsquedas " + tipo_busqueda + "\n")
                for longitud in sorted(self.tiempos_por_longitud[tipo_busqueda]):
                    tiempos = self.tiempos_por_longitud[tipo_busqueda][longitud]
                    min_tiempo = min(tiempos)
                    max_tiempo = max(tiempos)
                    prom_tiempo = sum(tiempos) / float(len(tiempos))
                    file_tiempos.write("--------------------\n")
                    file_tiempos.write(u"LONGITUD QUERY: " + str(longitud) + "\n")
                    file_tiempos.write(u"TIEMPO MÁXIMO: " + str(max_tiempo) + " segundos\n")
                    file_tiempos.write(u"TIEMPO MÍNIMO: " + str(min_tiempo) + " segundos\n")
                    file_tiempos.write(u"TIEMPO PROMEDIO: " + str(prom_tiempo) + " segundos\n")


def main(dir_indices, path_queries):
    buscador = Buscador(dir_indices)
    buscador.cargar_terminos()
    buscador.cargar_indice()
    buscador.cargar_idfs()
    buscador.cargar_normas_docs()
    parseador_queries = ParseadorQueries(path_queries)
    parseador_queries.cargar_queries()
    for index, query in enumerate(parseador_queries.queries):
        sys.stdout.write("\rProcesando la consulta Nº" + str(index + 1))
        sys.stdout.flush()
        buscador.cargar_busqueda(query)
    buscador.guardar_tiempos()
    print "\nFinalizado!"


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print u"ERROR: Debe ingresar el directorio que contiene los indices y el archivo con las consultas."
        print u"MODO DE USO: buscar_vectorial_ej10.py <path_indices> <path_queries>"
    else:
        main(sys.argv[1], sys.argv[2])
