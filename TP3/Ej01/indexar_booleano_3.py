# -*- coding: utf-8 -*-

import re
import codecs
from os import walk, makedirs, remove, listdir
from os.path import join, exists, getsize
from struct import Struct
from shutil import rmtree


class Tokenizador(object):
    def __init__(self, min_len, max_len, path_vacias=None):
        self.sacar_vacias = False if path_vacias is None else True
        self.vacias = None
        self.min_len = min_len
        self.max_len = max_len
        self.vocabulario = {}
        self.id_doc = 0
        self.cargar_lista_vacias(path_vacias)
        self.cantidad_terminos = 0

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    @staticmethod
    def tokenizar(texto):
        texto = texto.lower()
        texto = Tokenizador.translate(texto)
        texto = re.sub(u"[^a-zñ]|_", " ", texto)
        return texto.split()

    def cargar_lista_vacias(self, path_vacias):
        if self.sacar_vacias and path_vacias is not None:
            with codecs.open(path_vacias, mode="r", encoding="utf-8") as file_vacias:
                texto_vacias = file_vacias.read()
                self.vacias = self.tokenizar(texto_vacias)

    def analizar_documento(self, path_doc):
        with codecs.open(path_doc, mode="r", encoding="utf-8", errors="ignore") as file_doc:
            texto_doc = file_doc.read()
            tokens = self.tokenizar(texto_doc)
            if self.sacar_vacias:
                tokens = [token for token in tokens if token not in self.vacias]
            self.cargar_vocabulario(tokens)
            self.id_doc += 1

    def cargar_vocabulario(self, tokens):
        for token in tokens:
            if self.min_len < len(token) < self.max_len:
                if token not in self.vocabulario:
                    self.vocabulario[token] = {}
                if self.id_doc in self.vocabulario[token]:
                    self.vocabulario[token][self.id_doc] += 1
                else:
                    self.vocabulario[token][self.id_doc] = 1
                self.cantidad_terminos += 1


class Indexador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    PATH_POSTINGS = "postings"
    PATH_INDICES = "indices"
    NOMBRE_POSTINGS = "POS"
    NOMBRE_INDICES = "IDX"
    NOMBRE_TERMINOS = "TER.txt"
    EXT_BIN = ".bin"

    def __init__(self, path_corpus, limite_terminos_bloque):
        self.path_corpus = path_corpus
        self.limite_terminos_bloque = limite_terminos_bloque
        self.tokenizador = Tokenizador(3, 20)
        self.num_file_postings = 0
        self.terms = []
        self.packer_pos = Struct(self.FORMATO_POSTING)

    def guardar_terminos(self, terminos):
        with codecs.open(self.NOMBRE_TERMINOS, mode="w", encoding="utf-8") as file_terminos:
            for termino in terminos:
                file_terminos.write(termino + "\n")

    def guardar_postings(self, path_postings):
        with open(path_postings, mode="wb") as file_postings:
            for id_termino, termino in enumerate(self.terms):
                if termino in self.tokenizador.vocabulario:
                    file_postings.write(self.packer_pos.pack(id_termino))
                    file_postings.write(self.packer_pos.pack(len(self.tokenizador.vocabulario[termino])))
                    for id_doc in sorted(self.tokenizador.vocabulario[termino]):
                        file_postings.write(self.packer_pos.pack(id_doc))

    def analizar_corpus(self):
        self.inicializar_directorios()
        for raiz, dirs, nombres_docs in walk(self.path_corpus):
            for nombre_doc in nombres_docs:
                path_doc = join(raiz, nombre_doc)
                self.tokenizador.analizar_documento(path_doc)
                self.verificar_limite()
        if self.tokenizador.cantidad_terminos > 0:
            self.guardar_bloque_postings()
        self.tokenizador = None
        self.unir_bloques_postings()

    def inicializar_directorios(self):
        if exists(self.NOMBRE_POSTINGS + self.EXT_BIN):
            remove(self.NOMBRE_POSTINGS + self.EXT_BIN)
        if exists(self.NOMBRE_INDICES + self.EXT_BIN):
            remove(self.NOMBRE_INDICES + self.EXT_BIN)
        if exists(self.PATH_POSTINGS):
            rmtree(self.PATH_POSTINGS)

    def verificar_limite(self):
        if self.tokenizador.cantidad_terminos >= self.limite_terminos_bloque:
            self.guardar_bloque_postings()

    def guardar_bloque_postings(self):
        if not exists(self.PATH_POSTINGS):
            makedirs(self.PATH_POSTINGS)
        for termino in self.tokenizador.vocabulario:
            if termino not in self.terms:
                self.terms.append(termino)
        nombre_file_posting = self.NOMBRE_POSTINGS + str(self.num_file_postings).zfill(4) + self.EXT_BIN
        self.guardar_postings(join(self.PATH_POSTINGS, nombre_file_posting))
        self.tokenizador = Tokenizador(3, 20)
        self.num_file_postings += 1

    def unir_bloques_postings(self):
        terminos_ordenados = sorted((t, i) for i, t in enumerate(self.terms))
        id_ordenado = 0
        for termino, id_termino in terminos_ordenados:
            docs_termino = []
            for nombre_postings in listdir(self.PATH_POSTINGS):
                path_postings = join(self.PATH_POSTINGS, nombre_postings)
                with open(path_postings, mode="rb") as file_postings:
                    bytes_id = file_postings.read(self.packer_pos.size)
                    while bytes_id:
                        id_posting = self.packer_pos.unpack(bytes_id)[0]
                        df_posting = self.packer_pos.unpack(file_postings.read(self.packer_pos.size))[0]
                        if id_posting == id_termino:
                            for i in xrange(df_posting):
                                id_doc = self.packer_pos.unpack(file_postings.read(self.packer_pos.size))[0]
                                docs_termino.append(id_doc)
                            break
                        else:
                            file_postings.seek(df_posting * self.packer_pos.size, 1)
                            bytes_id = file_postings.read(self.packer_pos.size)
            self.cargar_postings_final(id_ordenado, docs_termino)
            id_ordenado += 1
        self.guardar_terminos(sorted(self.terms))

    def cargar_postings_final(self, id_termino, docs_termino):
        path_postings = self.NOMBRE_POSTINGS + self.EXT_BIN
        path_indice = self.NOMBRE_INDICES + self.EXT_BIN
        with open(path_postings, mode="ab") as file_postings:
            limite_termino = getsize(path_postings)
            for id_doc in sorted(docs_termino):
                file_postings.write(self.packer_pos.pack(id_doc))
                limite_termino += self.packer_pos.size
        packer_idx = Struct(self.FORMATO_INDICE)
        with open(path_indice, mode="ab") as file_indice:
            file_indice.write(packer_idx.pack(id_termino, limite_termino))


def main():
    indexador = Indexador("RI-tknz-data", 200000)
    indexador.analizar_corpus()

if __name__ == "__main__":
    main()
