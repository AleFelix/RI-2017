# -*- coding: utf-8 -*-

import codecs
from struct import Struct
from indexar_booleano import Tokenizador


class Buscador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    AND = "and"
    OR = "or"
    NOT = "not"

    def __init__(self, path_terminos, path_indice, path_postings):
        self.path_terminos = path_terminos
        self.path_indice = path_indice
        self.path_postings = path_postings
        self.terminos = []
        self.indice = {}

    def cargar_terminos(self):
        with codecs.open(self.path_terminos, mode="r", encoding="utf-8") as file_terminos:
            for termino in file_terminos:
                self.terminos.append(termino.strip())

    def cargar_indice(self):  # Formato del indice: [ID, byte donde termina la lista de docs]
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="rb") as file_indice:
            bytes_indice = file_indice.read(packer.size)
            while bytes_indice:
                elementos_indice = packer.unpack(bytes_indice)
                self.indice[elementos_indice[0]] = elementos_indice[1]
                bytes_indice = file_indice.read(packer.size)

    def cargar_postings(self, lista_ids_terminos):
        packer = Struct(self.FORMATO_POSTING)
        with open(self.path_postings, mode="rb") as file_postings:
            docs_por_termino = {}
            for id_termino in lista_ids_terminos:
                docs_por_termino[id_termino] = []
                if id_termino in self.indice:
                    fin_docs = self.indice[id_termino]
                    inicio_docs = self.indice[id_termino - 1] if id_termino > 0 else 0
                    file_postings.seek(inicio_docs)
                    while inicio_docs < fin_docs:
                        bytes_doc = file_postings.read(packer.size)
                        id_doc = packer.unpack(bytes_doc)[0]  # unpack() devuelve una tupla aunque haya un solo elemento
                        docs_por_termino[id_termino].append(id_doc)
                        inicio_docs += packer.size
            return docs_por_termino

    def obtener_ids_terminos(self, lista_terminos):
        lista_ids = []
        for termino in lista_terminos:
            if termino not in self.terminos:
                lista_ids.append(-1)
            else:
                lista_ids.append(self.terminos.index(termino))
        return lista_ids

    @staticmethod
    def unir(lista_1, lista_2):
        return sorted(list(set().union(lista_1, lista_2)))

    # noinspection PyUnresolvedReferences
    @staticmethod
    def intersectar(lista_1, lista_2):
        interseccion = []
        if len(lista_1) > len(lista_2):
            lista_1, lista_2 = lista_2, lista_1
        indice_1 = 0
        indice_2 = 0
        while indice_1 < len(lista_1):
            while indice_2 < len(lista_2) and lista_2[indice_2] < lista_1[indice_1]:
                indice_2 += 1
            if lista_1[indice_1] == lista_2[indice_2]:
                interseccion.append(lista_1[indice_1])
            indice_1 += 1
        return interseccion

    @staticmethod
    def restar(lista_1, lista_2):
        return sorted(list(set(lista_1) - set(lista_2)))

    def cargar_busqueda(self, lista_parametros):
        cant_params = len(lista_parametros)
        if cant_params == 0:
            print u"Debe ingresar al menos un término de busqueda"
        elif cant_params == 1 and lista_parametros[0] in [self.OR, self.AND, self.NOT]:
            print u"La busqueda no puede contener únicamente un operador sin términos"
        elif cant_params == 2:
            print u"No puede ingresar solamente dos paŕametros"
        elif cant_params == 3 and\
                (lista_parametros[0] in [self.OR, self.AND]
                 or lista_parametros[1] not in [self.OR, self.AND]
                 or lista_parametros[2] in [self.OR, self.AND]):
            print u"Si ingresa tres parámetros, debe ser en formato: 'término <or|and> término'"
        elif cant_params == 4 and \
                (lista_parametros[0] in [self.OR, self.AND, self.NOT]
                 or lista_parametros[1] != self.AND
                 or lista_parametros[2] != self.NOT
                 or lista_parametros[3] in [self.OR, self.AND, self.NOT]):
            print u"Si ingresa 4 parámetros, debe ser en formato: 'término and not término'"
        elif cant_params > 4:
            print u"Solo puede ingresar hasta 4 parámetros"
        else:
            respuesta = self.buscar(lista_parametros)
            print u"Resultados de la búsqueda"
            if respuesta:
                for id_doc in respuesta:
                    print id_doc
            else:
                print u"No se obtuvo ningún resultado"

    def buscar(self, lista_parametros):
        terminos_query = []
        for parametro in lista_parametros:
            if parametro not in [self.OR, self.AND, self.NOT]:
                terminos_query.append(parametro)
        lista_ids = self.obtener_ids_terminos(terminos_query)
        postings = self.cargar_postings(lista_ids)
        if len(lista_parametros) == 1:
            return postings[lista_ids[0]]
        else:
            for indice in xrange(len(lista_parametros)):
                if lista_parametros[indice] == self.OR:
                    return self.unir(postings[lista_ids[0]], postings[lista_ids[1]])
                if lista_parametros[indice] == self.AND:
                    if lista_parametros[indice+1] != self.NOT:
                        return self.intersectar(postings[lista_ids[0]], postings[lista_ids[1]])
                    else:
                        return self.restar(postings[lista_ids[0]], postings[lista_ids[1]])


def main():
    buscador = Buscador("terminos.txt", "indice.bin", "postings.bin")
    buscador.cargar_terminos()
    buscador.cargar_indice()
    texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")
    while texto_consulta != "/q":
        parametros_consulta = Tokenizador.tokenizar(texto_consulta)
        buscador.cargar_busqueda(parametros_consulta)
        texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")


if __name__ == "__main__":
    main()
