# -*- coding: utf-8 -*-

import re
import codecs
from os import walk
from os.path import join
from struct import Struct


class Tokenizador(object):
    def __init__(self, path_corpus, min_len, max_len, path_vacias=None):
        self.path_corpus = path_corpus
        self.sacar_vacias = False if path_vacias is None else True
        self.vacias = None
        self.min_len = min_len
        self.max_len = max_len
        self.vocabulario = {}
        self.id_doc = 0
        self.cantidad_docs = 0
        self.cargar_lista_vacias(path_vacias)

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    @staticmethod
    def tokenizar(texto):
        texto = texto.lower()
        texto = Tokenizador.translate(texto)
        texto = re.sub(u"[^a-zñ]|_", " ", texto)
        return texto.split()

    def cargar_lista_vacias(self, path_vacias):
        if self.sacar_vacias and path_vacias is not None:
            with codecs.open(path_vacias, mode="r", encoding="utf-8") as file_vacias:
                texto_vacias = file_vacias.read()
                self.vacias = self.tokenizar(texto_vacias)

    def analizar_corpus(self):
        for raiz, dirs, nombres_docs in walk(self.path_corpus):
            for nombre_doc in nombres_docs:
                path_doc = join(raiz, nombre_doc)
                self.analizar_documento(path_doc)
                self.id_doc += 1
        self.cantidad_docs = self.id_doc

    def analizar_documento(self, path_doc):
        with codecs.open(path_doc, mode="r", encoding="utf-8", errors="ignore") as file_doc:
            texto_doc = file_doc.read()
            tokens = self.tokenizar(texto_doc)
            if self.sacar_vacias:
                tokens = [token for token in tokens if token not in self.vacias]
            self.cargar_vocabulario(tokens)

    def cargar_vocabulario(self, tokens):
        for token in tokens:
            if self.min_len < len(token) < self.max_len:
                if token not in self.vocabulario:
                    self.vocabulario[token] = {}
                if self.id_doc in self.vocabulario[token]:
                    self.vocabulario[token][self.id_doc] += 1
                else:
                    self.vocabulario[token][self.id_doc] = 1


class Indexador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"

    def __init__(self, vocabulario, path_terminos, path_postings, path_indice):
        self.vocabulario = vocabulario
        self.path_terminos = path_terminos
        self.path_postings = path_postings
        self.path_indice = path_indice
        self.indice = {}
        self.terminos = sorted([termino for termino in self.vocabulario])

    def guardar_terminos(self):
        with codecs.open(self.path_terminos, mode="w", encoding="utf-8") as file_terminos:
            for termino in self.terminos:
                file_terminos.write(termino + "\n")

    def guardar_postings(self):
        packer = Struct(self.FORMATO_POSTING)
        limite_termino = 0
        with open(self.path_postings, mode="wb") as file_postings:
            for id_termino, termino in enumerate(self.terminos):
                # file_postings.write(packer.pack(id_termino))
                # limite_termino += packer.size
                for id_doc in sorted(self.vocabulario[termino]):
                    file_postings.write(packer.pack(id_doc))
                    limite_termino += packer.size
                self.indice[id_termino] = limite_termino

    def guardar_indice(self):
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="wb") as file_indice:
            for id_termino in sorted(self.indice):
                file_indice.write(packer.pack(id_termino, self.indice[id_termino]))


def main():
    tokenizador = Tokenizador("RI-tknz-data", 3, 20)
    tokenizador.analizar_corpus()
    indexador = Indexador(tokenizador.vocabulario, "terminos.txt", "postings.bin", "indice.bin")
    indexador.guardar_terminos()
    indexador.guardar_postings()
    indexador.guardar_indice()

if __name__ == "__main__":
    main()
