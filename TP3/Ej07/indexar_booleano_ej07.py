# -*- coding: utf-8 -*-

import re
import sys
import codecs
from os import walk, makedirs
from os.path import join, isdir
from struct import Struct


class Tokenizador(object):
    MIN_LEN_TERM = 3
    MAX_LEN_TERM = 20

    def __init__(self, path_corpus, min_len=MIN_LEN_TERM, max_len=MAX_LEN_TERM, path_vacias=None):
        self.path_corpus = path_corpus
        self.sacar_vacias = False if path_vacias is None else True
        self.vacias = None
        self.min_len = min_len
        self.max_len = max_len
        self.vocabulario = {}
        self.id_doc = 0
        self.cantidad_docs = 0
        self.nombres_docs = []
        self.nombre_doc_actual = None
        self.doc_actual_tiene_terminos = None
        self.cargar_lista_vacias(path_vacias)

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    @staticmethod
    def tokenizar(texto):
        texto = texto.lower()
        texto = Tokenizador.translate(texto)
        texto = re.sub(u"[^a-zñ]|_", " ", texto)
        return texto.split()

    def cargar_lista_vacias(self, path_vacias):
        if self.sacar_vacias and path_vacias is not None:
            with codecs.open(path_vacias, mode="r", encoding="utf-8") as file_vacias:
                texto_vacias = file_vacias.read()
                self.vacias = self.tokenizar(texto_vacias)

    def analizar_corpus(self):
        for raiz, dirs, nombres_docs in walk(unicode(self.path_corpus)):
            for nombre_doc in nombres_docs:
                if (self.id_doc + 1) % 100 == 0:
                    sys.stdout.write("\r" + str(self.id_doc + 1) + " documentos procesados...")
                    sys.stdout.flush()
                self.nombre_doc_actual = nombre_doc
                self.doc_actual_tiene_terminos = False
                path_doc = join(raiz, nombre_doc)
                self.analizar_documento(path_doc)
                if self.doc_actual_tiene_terminos:
                    self.id_doc += 1
        self.cantidad_docs = self.id_doc
        sys.stdout.write("\n")
        sys.stdout.flush()

    def analizar_documento(self, path_doc):
        with codecs.open(path_doc, mode="r", encoding="utf-8", errors="ignore") as file_doc:
            texto_doc = file_doc.read()
            tokens = self.tokenizar(texto_doc)
            if self.sacar_vacias:
                tokens = list(set(tokens) - set(self.vacias))
            self.cargar_vocabulario(tokens)

    def cargar_vocabulario(self, tokens):
        for token in tokens:
            if self.min_len < len(token) < self.max_len:
                if token not in self.vocabulario:
                    self.vocabulario[token] = {}
                if self.id_doc in self.vocabulario[token]:
                    self.vocabulario[token][self.id_doc] += 1
                else:
                    self.vocabulario[token][self.id_doc] = 1
                if not self.doc_actual_tiene_terminos:
                    self.nombres_docs.append(self.nombre_doc_actual)
                    self.doc_actual_tiene_terminos = True


class Indexador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FILE_TERMINOS = "terminos.txt"
    FILE_DOCUMENTOS = "documentos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    FILE_SKIPS = "skips.bin"
    INTERVALO_SKIP = 5

    def __init__(self, vocabulario, lista_documentos, dir_indices):
        self.crear_directorio(dir_indices)
        self.vocabulario = vocabulario
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_documentos = join(dir_indices, self.FILE_DOCUMENTOS)
        self.path_postings = join(dir_indices, self.FILE_POSTINGS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_skips = join(dir_indices, self.FILE_SKIPS)
        self.indice = {}
        self.terminos = sorted([termino for termino in self.vocabulario])
        self.documentos = lista_documentos
        self.skips = {}

    @staticmethod
    def crear_directorio(path_dir):
        try:
            makedirs(path_dir)
        except OSError:
            if not isdir(path_dir):
                raise

    def guardar_terminos(self):
        with codecs.open(self.path_terminos, mode="w", encoding="utf-8") as file_terminos:
            for termino in self.terminos:
                file_terminos.write(termino + "\n")

    def guardar_nombres_documentos(self):
        with codecs.open(self.path_documentos, mode="w", encoding="utf-8") as file_documentos:
            for nombre_doc in self.documentos:
                file_documentos.write(nombre_doc + "\n")

    def guardar_postings(self):
        packer = Struct(self.FORMATO_POSTING)
        pos_posting = 0
        with open(self.path_postings, mode="wb") as file_postings:
            for id_termino, termino in enumerate(self.terminos):
                self.skips[id_termino] = []
                self.indice[id_termino] = pos_posting
                df_termino = len(self.vocabulario[termino])
                file_postings.write(packer.pack(df_termino))
                pos_posting += packer.size
                for pos_doc, id_doc in enumerate(sorted(self.vocabulario[termino])):
                    file_postings.write(packer.pack(id_doc))
                    pos_posting += packer.size
                    if pos_doc % self.INTERVALO_SKIP == 0:
                        self.skips[id_termino].append((id_doc, pos_doc))

    def guardar_skips(self):
        packer = Struct(self.FORMATO_POSTING)
        with open(self.path_skips, mode="wb") as file_skips:
            for id_termino, termino in enumerate(self.terminos):
                file_skips.write(packer.pack(id_termino))
                file_skips.write(packer.pack(len(self.skips[id_termino])))
                for id_doc, pos_doc in self.skips[id_termino]:
                    file_skips.write(packer.pack(id_doc))
                    file_skips.write(packer.pack(pos_doc))

    def guardar_indice(self):
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="wb") as file_indice:
            for id_termino in sorted(self.indice):
                file_indice.write(packer.pack(id_termino, self.indice[id_termino]))


def main(dir_corpus, dir_indices, path_vacias=None):
    tokenizador = Tokenizador(dir_corpus, path_vacias=path_vacias)
    print u"Extrayendo los términos de la colección..."
    tokenizador.analizar_corpus()
    print u"Extracción terminada, generando los indices..."
    indexador = Indexador(tokenizador.vocabulario, tokenizador.nombres_docs, dir_indices)
    print u"Guardando los nombres de los términos..."
    indexador.guardar_terminos()
    print u"Guardando los nombres de los documentos..."
    indexador.guardar_nombres_documentos()
    print u"Guardando las posting-lists..."
    indexador.guardar_postings()
    print u"Guardando el indice..."
    indexador.guardar_indice()
    print u"Guardando las skips..."
    indexador.guardar_skips()
    print u"Finalizado!"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "ERROR: Debe ingresar el directorio que contiene el corpus y el directorio destino de los indices."
        print "MODO DE USO: indexar_booleano_ej07.py <path_corpus> <path_indices> [<path_vacias>]"
    else:
        if len(sys.argv) > 3:
            main(sys.argv[1], sys.argv[2], sys.argv[3])
        else:
            main(sys.argv[1], sys.argv[2])
