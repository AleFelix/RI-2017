# -*- coding: utf-8 -*-

import sys
import codecs
from struct import Struct
from os.path import join
from indexar_booleano_ej03 import Tokenizador


class Buscador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FILE_TERMINOS = "terminos.txt"
    FILE_DOCUMENTOS = "documentos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    AND = "and"
    OR = "or"
    NOT = "not"
    ERROR_CONSULTA = u"ERROR: La consulta debe tener el formato '[<not>] <término> [(<and>|<or>|<and not>) <término>]'"

    def __init__(self, dir_indices):
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_documentos = join(dir_indices, self.FILE_DOCUMENTOS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_postings = join(dir_indices, self.FILE_POSTINGS)
        self.terminos = []
        self.documentos = []
        self.indice = {}

    def cargar_terminos(self):  # Archivo de texto con los nombres de los términos separados por saltos de línea
        with codecs.open(self.path_terminos, mode="r", encoding="utf-8") as file_terminos:
            for termino in file_terminos:
                self.terminos.append(termino.strip())

    def cargar_documentos(self):  # Archivo de texto con los nombres de los documentos separados por saltos de línea
        with codecs.open(self.path_documentos, mode="r", encoding="utf-8") as file_documentos:
            for documento in file_documentos:
                self.documentos.append(documento.strip())

    def cargar_indice(self):  # Formato del indice: [id_término, byte donde empieza la posting del término]
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="rb") as file_indice:
            bytes_indice = file_indice.read(packer.size)
            while bytes_indice:
                elementos_indice = packer.unpack(bytes_indice)
                self.indice[elementos_indice[0]] = elementos_indice[1]
                bytes_indice = file_indice.read(packer.size)

    def cargar_postings(self, lista_ids_terminos):  # Formato de las postings: df_término, secuencia de id_docs
        packer = Struct(self.FORMATO_POSTING)
        with open(self.path_postings, mode="rb") as file_postings:
            docs_por_termino = {}
            for id_termino in lista_ids_terminos:
                docs_por_termino[id_termino] = []
                if id_termino in self.indice:
                    pos_posting = self.indice[id_termino]
                    file_postings.seek(pos_posting)
                    bytes_df = file_postings.read(packer.size)
                    df_termino = packer.unpack(bytes_df)[0]  # unpack() devuelve una tupla aunque haya un solo elemento
                    for i in xrange(df_termino):
                        bytes_doc = file_postings.read(packer.size)
                        id_doc = packer.unpack(bytes_doc)[0]
                        docs_por_termino[id_termino].append(id_doc)
            return docs_por_termino

    def obtener_ids_terminos(self, lista_terminos):
        lista_ids = []
        for termino in lista_terminos:
            if termino in self.terminos:
                lista_ids.append(self.terminos.index(termino))
        return lista_ids

    @staticmethod
    def unir(lista_1, lista_2):
        return sorted(list(set(lista_1) | set(lista_2)))

    @staticmethod
    def intersectar(lista_1, lista_2):
        return sorted(list(set(lista_1) & set(lista_2)))

    @staticmethod
    def restar(lista_1, lista_2):
        return sorted(list(set(lista_1) - set(lista_2)))

    def cargar_busqueda(self, params):
        cant_params = len(params)
        operadores = [self.OR, self.AND, self.NOT]
        if cant_params == 0:
            print u"ERROR: Debe ingresar al menos un término de busqueda"
        elif cant_params == 1 and params[0] in operadores:
            print u"ERROR: La busqueda no puede contener únicamente un operador sin términos"
        elif cant_params == 2:
            print self.ERROR_CONSULTA
        elif cant_params == 3 and (params[0] in operadores or params[1] not in [self.OR, self.AND] or
                                   params[2] in operadores):
            print self.ERROR_CONSULTA
        elif cant_params == 4 and ((params[0] != self.NOT and params[1] != self.AND and
                                    params[2] not in [self.AND, self.NOT]) or
                                   (params[0] == self.NOT and params[2] == self.AND and params[1] in operadores) or
                                   (params[1] == self.AND and params[2] == self.NOT and params[0] in operadores) or
                                   (params[0] == self.NOT and params[2] != self.AND) or
                                   (params[0] != self.NOT and params[2] == self.AND) or
                                   (params[1] == self.AND and params[2] != self.NOT) or
                                   (params[1] != self.AND and params[2] == self.NOT) or
                                   (params[3] in operadores)):
            print self.ERROR_CONSULTA
        elif cant_params > 4:
            print u"ERROR: Solo puede ingresar hasta 4 parámetros"
        else:
            respuesta = self.buscar(params)
            print u"Resultados de la búsqueda"
            if respuesta:
                print "ID_DOC\t\tNOMBRE_DOC"
                for id_doc in respuesta:
                    print str(id_doc) + "\t\t\t" + self.documentos[id_doc]
            else:
                print u"No se obtuvo ningún resultado"

    def buscar(self, lista_parametros):
        terminos_query = []
        for parametro in lista_parametros:
            if parametro not in [self.OR, self.AND, self.NOT]:
                terminos_query.append(parametro)
        lista_ids = self.obtener_ids_terminos(terminos_query)
        if not lista_ids:
            return None
        else:
            postings = self.cargar_postings(lista_ids)
            posting_1 = postings[lista_ids[0]]
            posting_2 = postings[lista_ids[1]] if len(lista_ids) > 1 else []
            cant_params = len(lista_parametros)
            if cant_params == 1:
                return posting_1
            elif cant_params == 3:
                if lista_parametros[1] == self.OR:
                    return self.unir(posting_1, posting_2)
                else:
                    return self.intersectar(posting_1, posting_2)
            else:
                if lista_parametros[0] == self.NOT:
                    return self.restar(posting_2, posting_1)
                else:
                    return self.restar(posting_1, posting_2)


def main(dir_indices):
    buscador = Buscador(dir_indices)
    buscador.cargar_terminos()
    buscador.cargar_documentos()
    buscador.cargar_indice()
    texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")
    while texto_consulta != "/q":
        parametros_consulta = Tokenizador.tokenizar(texto_consulta)
        buscador.cargar_busqueda(parametros_consulta)
        texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "ERROR: Debe ingresar el directorio que contiene los indices."
        print "MODO DE USO: buscar_booleano_ej03.py <path_indices>"
    else:
        main(sys.argv[1])
