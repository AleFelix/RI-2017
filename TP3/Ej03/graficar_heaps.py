# -*- coding: utf-8 -*-

import csv
from array import array
from struct import Struct
import matplotlib.pyplot as plt

FORMATO_HEAPS = "B"


def cargar_heaps_binario(path_heaps):
    packer = Struct(FORMATO_HEAPS)
    arreglo_heaps = array(FORMATO_HEAPS)
    with open(path_heaps, mode="rb") as file_heaps:
        byte_termino_unico = file_heaps.read(packer.size)
        while byte_termino_unico:
            valor_termino_unico = packer.unpack(byte_termino_unico)[0]
            arreglo_heaps.append(valor_termino_unico)
            byte_termino_unico = file_heaps.read(packer.size)
    return arreglo_heaps


def cargar_heaps_csv(path_heaps):
    with open(path_heaps, mode="rb") as file_csv:
        lista_unicos = []
        csv_reader = csv.reader(file_csv, delimiter=",")
        for fila in csv_reader:
            lista_unicos.append(fila[0])
    return lista_unicos


def graficar_heaps(unicos):
    plt.plot(xrange(1, len(unicos) + 1), unicos)
    plt.xlabel(u"Cantidad de términos encontrados")
    plt.ylabel(u"Cantidad de términos únicos")
    plt.grid(True)


def graficar_heaps_binario(arreglo_unicos):
    fig1, ax1 = plt.subplots()
    cantidad_terminos = 0
    cantidad_unicos = 0
    for byte_unico in arreglo_unicos:
        cantidad_terminos += 1
        cantidad_unicos += byte_unico
        ax1.scatter([cantidad_terminos], [cantidad_unicos])
    ax1.set_xlabel(u"Cantidad de términos encontrados")
    ax1.set_ylabel(u"Cantidad de términos únicos")
    ax1.grid(True)
    plt.show()


def calcular_estimacion_heaps(terminos_procesados, param_k, param_b):
    terminos_unicos = []
    for cantidad_procesada in terminos_procesados:
        terminos_unicos.append(param_k * pow(cantidad_procesada, param_b))
    return terminos_unicos


def graficar_predicciones_heaps(lista_predicciones, terminos_procesados, terminos_unicos, colores):
    figura, grafico = plt.subplots()
    for indice, prediccion in enumerate(lista_predicciones):
        grafico.plot(terminos_procesados, prediccion[2], color=colores[indice], linewidth=1, linestyle="--",
                     label="k: "+str(prediccion[0])+" | b: "+str(prediccion[1]))
    grafico.plot(terminos_procesados, terminos_unicos, color="b", linewidth=1, label="Real")
    grafico.legend(loc="upper left")
    grafico.grid(True)
    grafico.set_xlabel(u"Cantidad de términos encontrados")
    grafico.set_ylabel(u"Cantidad de términos únicos")
    figura.show()


def main():
    terminos_unicos = cargar_heaps_csv("ri-index/heaps.csv")
    terminos_unicos_2 = cargar_heaps_csv("ri-short/heaps.csv")
    terminos_encontrados = range(1, len(terminos_unicos) + 1)
    params_k = [10, 25, 50, 75, 100]
    params_b = [0.4, 0.5, 0.6]
    colores = ["r", "g", "y"]
    for param_k in params_k:
        lista_predicciones_heaps = []
        for param_b in params_b:
            lista_predicciones_heaps.append(
                [param_k, param_b, calcular_estimacion_heaps(terminos_encontrados, param_k, param_b)])
        graficar_predicciones_heaps(lista_predicciones_heaps, terminos_encontrados, terminos_unicos, colores)
    input("ok...")

if __name__ == "__main__":
    main()
