# -*- coding: utf-8 -*-

import re
import sys
import codecs
from struct import Struct
from os.path import join
from math import log, sqrt
from copy import deepcopy
from collections import Counter
from indexar_vectorial_ej06 import Tokenizador


class Buscador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FORMATO_PESO = "f"
    FORMATO_IDF = "f"
    FORMATO_POSICION = "H"
    FILE_TERMINOS = "terminos.txt"
    FILE_DOCUMENTOS = "documentos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    FILE_IDFS = "idfs.bin"
    FILE_NORMAS = "normas.bin"
    TIPO_TF_IDF = "TF-IDF"
    TIPO_TF_NORM = "TF-IDF-NORM"
    TIPO_TF_LOG = "TF-IDF-LOG"
    TOP_K = 10

    def __init__(self, dir_indices, dir_ponderacion):
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_documentos = join(dir_indices, self.FILE_DOCUMENTOS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_postings = join(dir_indices, dir_ponderacion, self.FILE_POSTINGS)
        self.path_normas = join(dir_indices, dir_ponderacion, self.FILE_NORMAS)
        self.path_idfs = join(dir_indices, self.FILE_IDFS)
        self.tipo_ponderacion = dir_ponderacion
        self.terminos = []
        self.documentos = []
        self.indice = {}
        self.idf_terminos = []
        self.normas_documentos = []

    def cargar_terminos(self):  # Archivo de texto con los nombres de los términos separados por saltos de línea
        with codecs.open(self.path_terminos, mode="r", encoding="utf-8") as file_terminos:
            for termino in file_terminos:
                self.terminos.append(termino.strip())

    def cargar_documentos(self):  # Archivo de texto con los nombres de los documentos separados por saltos de línea
        with codecs.open(self.path_documentos, mode="r", encoding="utf-8") as file_documentos:
            for documento in file_documentos:
                self.documentos.append(documento.strip())

    def cargar_indice(self):  # Formato del indice: [id_término, byte donde empieza la posting del término]
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="rb") as file_indice:
            bytes_indice = file_indice.read(packer.size)
            while bytes_indice:
                elementos_indice = packer.unpack(bytes_indice)
                self.indice[elementos_indice[0]] = elementos_indice[1]
                bytes_indice = file_indice.read(packer.size)

    def cargar_postings(self, lista_ids_terminos):  # Formato postings: df_term: id_doc, peso_term, tf_term: pos_term
        packer_post = Struct(self.FORMATO_POSTING)
        packer_peso = Struct(self.FORMATO_PESO)
        packer_pos = Struct(self.FORMATO_POSICION)
        with open(self.path_postings, mode="rb") as file_postings:
            docs_por_termino = {}
            for id_termino in lista_ids_terminos:
                docs_por_termino[id_termino] = {}
                if id_termino in self.indice:
                    pos_posting = self.indice[id_termino]
                    file_postings.seek(pos_posting)
                    bytes_df = file_postings.read(packer_post.size)
                    df_termino = packer_post.unpack(bytes_df)[0]
                    for i in xrange(df_termino):
                        bytes_doc = file_postings.read(packer_post.size)
                        id_doc = packer_post.unpack(bytes_doc)[0]
                        bytes_peso = file_postings.read(packer_peso.size)
                        peso_termino = packer_peso.unpack(bytes_peso)[0]
                        docs_por_termino[id_termino][id_doc] = {}
                        docs_por_termino[id_termino][id_doc]["peso"] = peso_termino
                        docs_por_termino[id_termino][id_doc]["posiciones"] = []
                        bytes_tf = file_postings.read(packer_post.size)
                        tf_termino = packer_post.unpack(bytes_tf)[0]
                        for j in xrange(tf_termino):
                            bytes_pos = file_postings.read(packer_pos.size)
                            pos_termino = packer_pos.unpack(bytes_pos)[0]
                            docs_por_termino[id_termino][id_doc]["posiciones"].append(pos_termino)
            return docs_por_termino

    def cargar_idfs(self):  # Formato: [idf_término]
        packer = Struct(self.FORMATO_IDF)
        with open(self.path_idfs, mode="rb") as file_idfs:
            bytes_idf = file_idfs.read(packer.size)
            while bytes_idf:
                idf_termino = packer.unpack(bytes_idf)[0]
                self.idf_terminos.append(idf_termino)
                bytes_idf = file_idfs.read(packer.size)

    def cargar_normas_docs(self):  # Formato: [norma_doc]
        packer = Struct(self.FORMATO_PESO)
        with open(self.path_normas, mode="rb") as file_normas:
            bytes_norma = file_normas.read(packer.size)
            while bytes_norma:
                norma_doc = packer.unpack(bytes_norma)[0]
                self.normas_documentos.append(norma_doc)
                bytes_norma = file_normas.read(packer.size)

    def obtener_ids_terminos(self, lista_terminos):
        lista_ids = []
        for termino in lista_terminos:
            if termino in self.terminos:
                lista_ids.append(self.terminos.index(termino))
        return sorted(lista_ids)

    def obtener_ids_terminos_frase(self, lista_terminos):
        lista_ids = []
        for termino in lista_terminos:
            if termino in self.terminos:
                lista_ids.append(self.terminos.index(termino))
            else:
                lista_ids.append(-1)
        return lista_ids

    def calcular_peso_termino(self, id_termino, tf_termino, max_tf_query):
        if self.tipo_ponderacion == self.TIPO_TF_IDF:
            return tf_termino * self.idf_terminos[id_termino]
        elif self.tipo_ponderacion == self.TIPO_TF_NORM:
            return (tf_termino / float(max_tf_query)) * self.idf_terminos[id_termino]
        elif self.tipo_ponderacion == self.TIPO_TF_LOG:
            return (1 + log(tf_termino)) * self.idf_terminos[id_termino]

    # noinspection PyTypeChecker
    def cargar_busqueda(self, params, terminos_por_frase):
        if len(params) == 0:
            print u"ERROR: Debe ingresar al menos un término de busqueda"
        else:
            pesos_terminos_query, lista_ids_frases = self.calcular_pesos_query(params, terminos_por_frase)
            score_docs = self.buscar(pesos_terminos_query, lista_ids_frases)
            print u"Resultados de la búsqueda"
            if score_docs:
                print "ID_DOC\t\tSCORE\t\t\tNOMBRE_DOC"
                for pos, id_doc in enumerate(sorted(score_docs, key=score_docs.get, reverse=True)):
                    print str(id_doc) + "\t\t" + str(score_docs[id_doc]) + "\t\t\t" + self.documentos[id_doc]
                    if pos + 1 >= self.TOP_K:
                        break
            else:
                print u"No se obtuvo ningún resultado"

    def calcular_pesos_query(self, lista_terminos, terminos_por_frase):
        lista_ids = self.obtener_ids_terminos(lista_terminos)
        lista_ids_frases = []
        if not lista_ids:
            return None, None
        else:
            if terminos_por_frase:
                for terminos_frase in terminos_por_frase:
                    ids_frase = self.obtener_ids_terminos_frase(terminos_frase)
                    lista_ids_frases.append(ids_frase)
            contador_ids = Counter(lista_ids)
            ids_unicos = contador_ids.keys()
            max_tf_query = max(contador_ids.values())
            pesos_terminos_query = {}
            for id_termino in ids_unicos:
                peso_termino = self.calcular_peso_termino(id_termino, contador_ids[id_termino], max_tf_query)
                pesos_terminos_query[id_termino] = peso_termino
            return pesos_terminos_query, lista_ids_frases

    @staticmethod
    def calcular_norma(lista_pesos):
        return sqrt(sum(pow(peso, 2) for peso in lista_pesos))

    def buscar(self, pesos_terminos_query, lista_ids_frases):
        if not pesos_terminos_query:
            return None
        lista_ids_terminos = [id_termino for id_termino in pesos_terminos_query]
        lista_pesos_terminos = [pesos_terminos_query[id_termino] for id_termino in pesos_terminos_query]
        postings = self.cargar_postings(lista_ids_terminos)
        if lista_ids_frases:
            for ids_frase in lista_ids_frases:
                for id_termino in ids_frase:
                    if id_termino not in postings:
                        return None
            postings = self.filtrar_postings_por_frases(postings, lista_ids_frases)
        if not postings:
            return None
        norma_query = self.calcular_norma(lista_pesos_terminos)
        score_documentos = {}
        for id_termino in lista_ids_terminos:
            for id_doc in postings[id_termino]:
                if id_doc not in score_documentos:
                    score_documentos[id_doc] = 0
                score_documentos[id_doc] += pesos_terminos_query[id_termino] * postings[id_termino][id_doc]["peso"]
        for id_doc in score_documentos:
            norma_doc = self.normas_documentos[id_doc]
            score_documentos[id_doc] /= float(norma_doc * norma_query)
            score_documentos[id_doc] = round(score_documentos[id_doc], 5)
        return score_documentos

    @staticmethod
    def filtrar_postings_por_frases(postings, lista_ids_frases):
        postings_filtradas = deepcopy(postings)
        for ids_frase in lista_ids_frases:
            if ids_frase:
                for id_doc in postings[ids_frase[0]]:
                    pos_validas_doc = None
                    for offset, id_termino in enumerate(ids_frase):
                        if id_doc in postings[id_termino]:
                            pos_validas_term = set([pos - offset for pos in postings[id_termino][id_doc]["posiciones"]])
                        else:
                            pos_validas_doc = set([])
                            break
                        if pos_validas_doc is not None:
                            pos_validas_doc = pos_validas_doc & pos_validas_term
                        else:
                            pos_validas_doc = pos_validas_term
                        if not pos_validas_doc:
                            break
                    if not pos_validas_doc:
                        for id_termino in ids_frase:
                            postings_filtradas[id_termino].pop(id_doc, None)
                if not postings_filtradas[ids_frase[0]]:
                    for id_termino in ids_frase:
                        for id_doc in postings[id_termino]:
                            postings_filtradas[id_termino].pop(id_doc, None)
        return postings_filtradas


def main(dir_indices, dir_ponderacion):
    buscador = Buscador(dir_indices, dir_ponderacion)
    buscador.cargar_terminos()
    buscador.cargar_documentos()
    buscador.cargar_indice()
    buscador.cargar_idfs()
    buscador.cargar_normas_docs()
    texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")
    while texto_consulta != "/q":
        lista_frases = re.findall(u"\"(.*?)\"", texto_consulta)
        terminos_por_frase = []
        for frase in lista_frases:
            terminos_frase = Tokenizador.tokenizar(frase)
            terminos_por_frase.append(terminos_frase)
        terminos_consulta = Tokenizador.tokenizar(texto_consulta)
        buscador.cargar_busqueda(terminos_consulta, terminos_por_frase)
        texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print u"ERROR: Debe ingresar el directorio que contiene los indices."
        print u"MODO DE USO: buscar_vectorial_ej06.py <path_indices> <tipo_ponderación>"
        print u"Tipos de ponderación: TF-IDF, TF-IDF-NORM, TF-IDF-LOG"
    else:
        if sys.argv[2].lower() == Buscador.TIPO_TF_IDF.lower():
            main(sys.argv[1], Buscador.TIPO_TF_IDF)
        elif sys.argv[2].lower() == Buscador.TIPO_TF_NORM.lower():
            main(sys.argv[1], Buscador.TIPO_TF_NORM)
        elif sys.argv[2].lower() == Buscador.TIPO_TF_LOG.lower():
            main(sys.argv[1], Buscador.TIPO_TF_LOG)
        else:
            print u"ERROR: El tipo de ponderación es invalido."
            print u"Tipos de ponderación: TF-IDF, TF-IDF-NORM, TF-IDF-LOG"
