# -*- coding: utf-8 -*-

import re
import sys
import codecs
from array import array
from math import log, sqrt
from os import walk, makedirs
from os.path import join, isdir
from struct import Struct


class Tokenizador(object):
    MIN_LEN_TERM = 0
    MAX_LEN_TERM = 20
    FORMATO_POSICION = "H"

    def __init__(self, path_corpus, min_len=MIN_LEN_TERM, max_len=MAX_LEN_TERM, path_vacias=None):
        self.path_corpus = path_corpus
        self.sacar_vacias = False if path_vacias is None else True
        self.vacias = None
        self.min_len = min_len
        self.max_len = max_len
        self.vocabulario = {}
        self.id_doc = 0
        self.cantidad_docs = 0
        self.nombres_docs = []
        self.nombre_doc_actual = None
        self.doc_actual_tiene_terminos = None
        self.pos_actual_term = None
        self.cargar_lista_vacias(path_vacias)
        self.max_tf_por_doc = []

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    @staticmethod
    def tokenizar(texto):
        texto = texto.lower()
        texto = Tokenizador.translate(texto)
        texto = re.sub(u"[^a-zñ]|_", " ", texto)
        return texto.split()

    def cargar_lista_vacias(self, path_vacias):
        if self.sacar_vacias and path_vacias is not None:
            with codecs.open(path_vacias, mode="r", encoding="utf-8") as file_vacias:
                texto_vacias = file_vacias.read()
                self.vacias = self.tokenizar(texto_vacias)

    def analizar_corpus(self):
        for raiz, dirs, nombres_docs in walk(unicode(self.path_corpus)):
            for nombre_doc in nombres_docs:
                if (self.id_doc + 1) % 100 == 0:
                    sys.stdout.write("\r" + str(self.id_doc + 1) + " documentos procesados...")
                    sys.stdout.flush()
                self.nombre_doc_actual = nombre_doc
                self.doc_actual_tiene_terminos = False
                self.pos_actual_term = 0
                self.max_tf_por_doc.append(0)
                path_doc = join(raiz, nombre_doc)
                self.analizar_documento(path_doc)
                if self.doc_actual_tiene_terminos:
                    self.id_doc += 1
        self.cantidad_docs = self.id_doc
        sys.stdout.write("\n")
        sys.stdout.flush()

    def analizar_documento(self, path_doc):
        with codecs.open(path_doc, mode="r", encoding="utf-8", errors="ignore") as file_doc:
            texto_doc = file_doc.read()
            tokens = self.tokenizar(texto_doc)
            if self.sacar_vacias:
                tokens = list(set(tokens) - set(self.vacias))
            self.cargar_vocabulario(tokens)

    def cargar_vocabulario(self, tokens):
        for token in tokens:
            if self.min_len < len(token) < self.max_len:
                if token not in self.vocabulario:
                    self.vocabulario[token] = {}
                if self.id_doc in self.vocabulario[token]:
                    self.vocabulario[token][self.id_doc].append(self.pos_actual_term)
                else:
                    self.vocabulario[token][self.id_doc] = array(self.FORMATO_POSICION, [self.pos_actual_term])
                if not self.doc_actual_tiene_terminos:
                    self.nombres_docs.append(self.nombre_doc_actual)
                    self.doc_actual_tiene_terminos = True
                if len(self.vocabulario[token][self.id_doc]) > self.max_tf_por_doc[self.id_doc]:
                    self.max_tf_por_doc[self.id_doc] = len(self.vocabulario[token][self.id_doc])
                self.pos_actual_term += 1


class Indexador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FORMATO_PESO = "f"
    FORMATO_IDF = "f"
    FORMATO_POSICION = "H"
    FILE_TERMINOS = "terminos.txt"
    FILE_DOCUMENTOS = "documentos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    FILE_IDFS = "idfs.bin"
    FILE_NORMAS = "normas.bin"
    TIPO_TF_IDF = "TF-IDF"
    TIPO_TF_NORM = "TF-IDF-NORM"
    TIPO_TF_LOG = "TF-IDF-LOG"

    def __init__(self, vocabulario, lista_documentos, max_tf_por_doc, dir_indices):
        self.vocabulario = vocabulario
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_documentos = join(dir_indices, self.FILE_DOCUMENTOS)
        self.paths_postings = {}
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_idfs = join(dir_indices, self.FILE_IDFS)
        self.paths_normas = {}
        self.indice = {}
        self.terminos = sorted([termino for termino in self.vocabulario])
        self.documentos = lista_documentos
        self.max_tf_por_doc = max_tf_por_doc
        self.idf_terminos = []
        self.pesos_terminos_por_doc = None
        self.crear_directorios_ponderaciones(dir_indices)

    def crear_directorios_ponderaciones(self, dir_indices):
        lista_dirs = [self.TIPO_TF_IDF, self.TIPO_TF_NORM, self.TIPO_TF_LOG]
        for dir_tf in lista_dirs:
            path_dir = join(dir_indices, dir_tf)
            self.crear_directorio(path_dir)
            self.paths_postings[dir_tf] = join(path_dir, self.FILE_POSTINGS)
            self.paths_normas[dir_tf] = join(path_dir, self.FILE_NORMAS)

    @staticmethod
    def crear_directorio(path_dir):
        try:
            makedirs(path_dir)
        except OSError:
            if not isdir(path_dir):
                raise

    def guardar_terminos(self):
        with codecs.open(self.path_terminos, mode="w", encoding="utf-8") as file_terminos:
            for termino in self.terminos:
                file_terminos.write(termino + "\n")

    def guardar_nombres_documentos(self):
        with codecs.open(self.path_documentos, mode="w", encoding="utf-8") as file_documentos:
            for nombre_doc in self.documentos:
                file_documentos.write(nombre_doc + "\n")

    def guardar_postings(self, tipo_ponderacion):
        packer_post = Struct(self.FORMATO_POSTING)
        packer_peso = Struct(self.FORMATO_PESO)
        packer_pos = Struct(self.FORMATO_POSICION)
        pos_posting = 0
        self.pesos_terminos_por_doc = {}
        with open(self.paths_postings[tipo_ponderacion], mode="wb") as file_postings:
            for id_termino, termino in enumerate(self.terminos):
                if id_termino not in self.indice:
                    self.indice[id_termino] = pos_posting
                df_termino = len(self.vocabulario[termino])
                file_postings.write(packer_post.pack(df_termino))
                pos_posting += packer_post.size
                for id_doc in sorted(self.vocabulario[termino]):
                    file_postings.write(packer_post.pack(id_doc))
                    pos_posting += packer_post.size
                    peso_termino = self.calcular_peso(termino, id_termino, id_doc, tipo_ponderacion)
                    file_postings.write(packer_peso.pack(peso_termino))
                    pos_posting += packer_peso.size
                    tf_termino = len(self.vocabulario[termino][id_doc])
                    file_postings.write(packer_post.pack(tf_termino))
                    pos_posting += packer_post.size
                    for pos_termino in self.vocabulario[termino][id_doc]:
                        file_postings.write(packer_pos.pack(pos_termino))
                        pos_posting += packer_pos.size
                    if id_doc in self.pesos_terminos_por_doc:
                        self.pesos_terminos_por_doc[id_doc].append(peso_termino)
                    else:
                        self.pesos_terminos_por_doc[id_doc] = [peso_termino]

    def guardar_indice(self):
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="wb") as file_indice:
            for id_termino in sorted(self.indice):
                file_indice.write(packer.pack(id_termino, self.indice[id_termino]))

    def guardar_idfs(self):
        packer = Struct(self.FORMATO_IDF)
        with open(self.path_idfs, mode="wb") as file_idfs:
            for id_termino, termino in enumerate(self.terminos):
                idf_termino = self.idf_terminos[id_termino]
                file_idfs.write(packer.pack(idf_termino))

    def calcular_peso(self, termino, id_termino, id_doc, tipo_ponderacion):
        tf = len(self.vocabulario[termino][id_doc])
        idf = self.idf_terminos[id_termino]
        if tipo_ponderacion == self.TIPO_TF_IDF:
            return float(tf * idf)
        if tipo_ponderacion == self.TIPO_TF_LOG:
            return float((1 + log(tf)) * idf)
        if tipo_ponderacion == self.TIPO_TF_NORM:
            return float((tf / float(self.max_tf_por_doc[id_doc])) * idf)

    def calcular_idfs(self):
        tam_corpus = len(self.documentos)
        for id_termino, termino in enumerate(self.terminos):
            df_termino = len(self.vocabulario[termino])
            idf_termino = tam_corpus / float(df_termino)
            self.idf_terminos.append(idf_termino)

    def guardar_normas_documentos(self, tipo_ponderacion):
        packer = Struct(self.FORMATO_PESO)
        with open(self.paths_normas[tipo_ponderacion], mode="wb") as file_normas:
            for id_doc, nombre_doc in enumerate(self.documentos):
                norma_doc = sqrt(sum([pow(peso, 2) for peso in self.pesos_terminos_por_doc[id_doc]]))
                file_normas.write(packer.pack(norma_doc))


def main(dir_corpus, dir_indices, path_vacias=None):
    tokenizador = Tokenizador(dir_corpus, path_vacias=path_vacias)
    print u"Extrayendo los términos de la colección..."
    tokenizador.analizar_corpus()
    print u"Extracción terminada, generando los indices..."
    indexador = Indexador(tokenizador.vocabulario, tokenizador.nombres_docs, tokenizador.max_tf_por_doc, dir_indices)
    print u"Guardando los nombres de los términos..."
    indexador.guardar_terminos()
    print u"Guardando los nombres de los documentos..."
    indexador.guardar_nombres_documentos()
    print u"Calculando los idfs de los términos..."
    indexador.calcular_idfs()
    for tipo_ponderacion in indexador.paths_postings:
        print u"Guardando las posting-lists con la ponderación: " + str(tipo_ponderacion) + "..."
        indexador.guardar_postings(tipo_ponderacion)
        print u"Guardando las normas con la ponderación: " + str(tipo_ponderacion) + "..."
        indexador.guardar_normas_documentos(tipo_ponderacion)
    print u"Guardando el indice..."
    indexador.guardar_indice()
    print u"Guardando los idfs..."
    indexador.guardar_idfs()
    print u"Finalizado!"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "ERROR: Debe ingresar el directorio que contiene el corpus y el directorio destino de los indices."
        print "MODO DE USO: indexar_vectorial_ej06.py <path_corpus> <path_indices>"
    else:
        if len(sys.argv) > 3:
            main(sys.argv[1], sys.argv[2], sys.argv[3])
        else:
            main(sys.argv[1], sys.argv[2])
