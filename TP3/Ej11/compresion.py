from math import log
from IntegerCodes import dec_to_bin, bin_to_dec


def codificar_numero_vl(num):
    bytes_num = []
    while True:
        bytes_num.insert(0, num % 128)
        if num < 128:
            break
        num = num / 128
    bytes_num[len(bytes_num) - 1] += 128
    return bytes_num


def codificar_numero_vl_str(num):
    lista_bytes = codificar_numero_vl(num)
    str_total_bits = ""
    for byte in lista_bytes:
        str_bits = str(dec_to_bin(byte, 8))
        str_total_bits += str_bits
    return str_total_bits


def codificar_numeros_vl(numeros):
    bytes_numeros = []
    for num in numeros:
        bytes_num = codificar_numero_vl(num)
        bytes_numeros.extend(bytes_num)
    return bytes_numeros


def decodificar_numeros_vl(bytes_numeros):
    numeros = []
    num = 0
    for i in xrange(len(bytes_numeros)):
        if bytes_numeros[i] < 128:
            num = 128 * num + bytes_numeros[i]
        else:
            num = 128 * num + (bytes_numeros[i] - 128)
            numeros.append(num)
            num = 0
    return numeros


def decodificar_numeros_vl_str(str_bits_numeros):
    int_numeros = []
    for i in xrange(0, len(str_bits_numeros), 8):
        str_bits_num = str_bits_numeros[i:i+8]
        int_numeros.append(convertir_a_int(str_bits_num))
    return decodificar_numeros_vl(int_numeros)


def codificar_unario(num):
    return int("1" * num + "0")


def convertir_a_bin(num):
    if num == 0:
        cantidad_bits_conversion = 1
    else:
        cantidad_bits_conversion = int(log(num, 2)) + 1
    return dec_to_bin(num, cantidad_bits_conversion)


def convertir_a_int(bits):
    str_bits = str(bits)
    return bin_to_dec(list(str_bits), len(str_bits), 0)


def codificar_numero_egamma(num):
    str_num_bin = str(convertir_a_bin(num))
    str_offset = str_num_bin[1:]
    len_offset_unario = codificar_unario(len(str_offset))
    return str(len_offset_unario) + str_offset


def codificar_numeros_egamma(numeros):
    str_bits_numeros = ""
    for num in numeros:
        str_bits_num = codificar_numero_egamma(num)
        str_bits_numeros = str_bits_numeros + str_bits_num
    return str_bits_numeros


def decodificar_numeros_egamma(str_bits_numeros):
    lista_numeros = []
    cantidad_bits = 0
    bits_num = ""
    flag_bits = False
    for bit in str_bits_numeros:
        if not flag_bits:
            if bit == "1":
                cantidad_bits += 1
            else:
                if cantidad_bits > 0:
                    flag_bits = True
                else:
                    num = 1
                    lista_numeros.append(num)
        else:
            if cantidad_bits > 0:
                bits_num = bits_num + bit
                cantidad_bits -= 1
            if cantidad_bits == 0:
                bits_num = int("1" + bits_num)
                num = convertir_a_int(bits_num)
                lista_numeros.append(num)
                bits_num = ""
                flag_bits = False
    if flag_bits and cantidad_bits == 0:
        bits_num = int("1" + bits_num)
        num = convertir_a_int(bits_num)
        lista_numeros.append(num)
    return lista_numeros
