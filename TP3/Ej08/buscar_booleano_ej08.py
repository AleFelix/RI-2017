# -*- coding: utf-8 -*-

import re
import sys
import time
import codecs
from struct import Struct
from os.path import join
from BTrees.OOBTree import OOBTree


def translate(to_translate):
    tabin = u"áéíóú"
    tabout = u"aeiou"
    tabin = [ord(char) for char in tabin]
    translate_table = dict(zip(tabin, tabout))
    return to_translate.translate(translate_table)


def tokenizar(texto):
    texto = texto.lower()
    texto = translate(texto)
    texto = re.sub(u"[^a-zñ0-9]|_", " ", texto)
    return texto.split()


class Buscador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FILE_TERMINOS = "terminos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    FILE_SKIPS = "skips.bin"
    AND = "and"
    OR = "or"
    NOT = "not"
    MODO_BINARIO = "binario"
    MODO_ARBOL = "arbol"
    ERROR_CONSULTA = u"ERROR: La consulta debe tener el formato '[<not>] <término> [(<and>|<or>|<and not>) <término>]'"

    def __init__(self, dir_indices):
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_postings = join(dir_indices, self.FILE_POSTINGS)
        self.path_skips = join(dir_indices, self.FILE_SKIPS)
        self.terminos = []
        self.indice = {}
        self.skips = {}
        self.arbol_b = OOBTree()

    def cargar_terminos(self):  # Archivo de texto con los nombres de los términos separados por saltos de línea
        with codecs.open(self.path_terminos, mode="r", encoding="utf-8") as file_terminos:
            for termino in file_terminos:
                self.terminos.append(termino.strip())

    def cargar_indice(self):  # Formato del indice: [id_término, byte donde empieza la posting del término]
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="rb") as file_indice:
            bytes_indice = file_indice.read(packer.size)
            while bytes_indice:
                elementos_indice = packer.unpack(bytes_indice)
                self.indice[elementos_indice[0]] = elementos_indice[1]
                bytes_indice = file_indice.read(packer.size)

    def cargar_skips(self):  # Formato: [id_término, cant_skips, lista de (id_doc, pos_doc)]
        packer = Struct(self.FORMATO_POSTING)
        with open(self.path_skips, mode="rb") as file_skips:
            bytes_id_term = file_skips.read(packer.size)
            while bytes_id_term:
                id_termino = packer.unpack(bytes_id_term)[0]
                self.skips[id_termino] = []
                bytes_cant_skips = file_skips.read(packer.size)
                cant_skips = packer.unpack(bytes_cant_skips)[0]
                for i in xrange(cant_skips):
                    bytes_id_doc = file_skips.read(packer.size)
                    id_doc = packer.unpack(bytes_id_doc)[0]
                    bytes_pos_doc = file_skips.read(packer.size)
                    pos_doc = packer.unpack(bytes_pos_doc)[0]
                    self.skips[id_termino].append((id_doc, pos_doc))
                bytes_id_term = file_skips.read(packer.size)

    def cargar_postings_dgaps(self, lista_ids_terminos):  # Formato: df_termino, dgaps
        packer = Struct(self.FORMATO_POSTING)
        with open(self.path_postings, mode="rb") as file_postings:
            dgaps_por_termino = {}
            for id_termino in lista_ids_terminos:
                dgaps_por_termino[id_termino] = []
                if id_termino in self.indice:
                    pos_posting = self.indice[id_termino]
                    file_postings.seek(pos_posting)
                    bytes_df = file_postings.read(packer.size)
                    df_termino = packer.unpack(bytes_df)[0]  # unpack() devuelve una tupla aunque haya un solo elemento
                    for i in xrange(df_termino):
                        bytes_dgap = file_postings.read(packer.size)
                        dgap = packer.unpack(bytes_dgap)[0]
                        dgaps_por_termino[id_termino].append(dgap)
            return dgaps_por_termino

    def obtener_ids_terminos(self, lista_terminos):
        lista_ids = []
        for termino in lista_terminos:
            if termino in self.terminos:
                lista_ids.append(self.terminos.index(termino))
        return lista_ids

    @staticmethod
    def unir(lista_1, lista_2):
        return sorted(list(set(lista_1) | set(lista_2)))

    @staticmethod
    def intersectar_skips(posting_1, posting_2, skips_1, skips_2):
        interseccion = []
        indice_p1 = 0
        indice_p2 = 0
        indice_s1 = 0
        indice_s2 = 0
        while indice_p1 < len(posting_1) and indice_p2 < len(posting_2):
            if posting_1[indice_p1] == posting_2[indice_p2]:
                interseccion.append(posting_1[indice_p1])
                indice_p1 += 1
                indice_p2 += 1
            elif posting_1[indice_p1] < posting_2[indice_p2]:
                if indice_s1 < len(skips_1) and skips_1[indice_s1][0] <= posting_2[indice_p2]:
                    while indice_s1 < len(skips_1) and skips_1[indice_s1][0] <= posting_2[indice_p2]:
                        indice_p1 = skips_1[indice_s1][1]
                        indice_s1 += 1
                else:
                    indice_p1 += 1
            elif indice_s2 < len(skips_2) and skips_2[indice_s2][0] <= posting_1[indice_p1]:
                while indice_s2 < len(skips_2) and skips_2[indice_s2][0] <= posting_1[indice_p1]:
                    indice_p2 = skips_2[indice_s2][1]
                    indice_s2 += 1
            else:
                indice_p2 += 1
        return interseccion

    @staticmethod
    def restar(lista_1, lista_2):
        return sorted(list(set(lista_1) - set(lista_2)))

    def cargar_busqueda(self, params, modo_busqueda):
        cant_params = len(params)
        operadores = [self.OR, self.AND, self.NOT]
        if cant_params == 0:
            print u"ERROR: Debe ingresar al menos un término de busqueda"
        elif cant_params == 1 and params[0] in operadores:
            print u"ERROR: La busqueda no puede contener únicamente un operador sin términos"
        elif cant_params == 2:
            print self.ERROR_CONSULTA
        elif cant_params == 3 and (params[0] in operadores or params[1] not in [self.OR, self.AND] or
                                   params[2] in operadores):
            print self.ERROR_CONSULTA
        elif cant_params == 4 and ((params[0] != self.NOT and params[1] != self.AND and
                                    params[2] not in [self.AND, self.NOT]) or
                                   (params[0] == self.NOT and params[2] == self.AND and params[1] in operadores) or
                                   (params[1] == self.AND and params[2] == self.NOT and params[0] in operadores) or
                                   (params[0] == self.NOT and params[2] != self.AND) or
                                   (params[0] != self.NOT and params[2] == self.AND) or
                                   (params[1] == self.AND and params[2] != self.NOT) or
                                   (params[1] != self.AND and params[2] == self.NOT) or
                                   (params[3] in operadores)):
            print self.ERROR_CONSULTA
        elif cant_params > 4:
            print u"ERROR: Solo puede ingresar hasta 4 parámetros"
        else:
            respuesta = self.buscar(params, modo_busqueda)
            return respuesta

    def buscar(self, lista_parametros, modo_busqueda):
        terminos_query = []
        for parametro in lista_parametros:
            if parametro not in [self.OR, self.AND, self.NOT]:
                terminos_query.append(parametro)
        lista_ids = self.obtener_ids_terminos(terminos_query)
        if not lista_ids:
            return None
        else:
            if modo_busqueda == self.MODO_BINARIO:
                postings = self.cargar_postings_dgaps(lista_ids)
            else:
                postings = self.cargar_postings_desde_arbol(lista_ids)
            posting_1_dgaps = postings[lista_ids[0]]
            posting_2_dgaps = postings[lista_ids[1]] if len(lista_ids) > 1 else []
            posting_1 = self.procesar_dgaps(posting_1_dgaps)
            posting_2 = self.procesar_dgaps(posting_2_dgaps)
            skips_1 = self.skips[lista_ids[0]]
            skips_2 = self.skips[lista_ids[1]] if len(lista_ids) > 1 else []
            cant_params = len(lista_parametros)
            if cant_params == 1:
                return posting_1
            elif cant_params == 3:
                if lista_parametros[1] == self.OR:
                    return self.unir(posting_1, posting_2)
                else:
                    return self.intersectar_skips(posting_1, posting_2, skips_1, skips_2)
            else:
                if lista_parametros[0] == self.NOT:
                    return self.restar(posting_2, posting_1)
                else:
                    return self.restar(posting_1, posting_2)

    @staticmethod
    def procesar_dgaps(posting_dgaps):
        posting = []
        for dgap in posting_dgaps:
            if not posting:
                posting = [dgap]
            else:
                posting.append(posting[-1] + dgap)
        return posting

    def cargar_arbol_b(self):
        self.arbol_b.update(self.cargar_postings_dgaps([id_termino for id_termino in self.indice]))

    def cargar_postings_desde_arbol(self, lista_ids):
        postings = {}
        for id_termino in lista_ids:
            postings[id_termino] = self.arbol_b[id_termino]
        return postings


def main(dir_indices):
    buscador = Buscador(dir_indices)
    print u"Cargando los términos..."
    buscador.cargar_terminos()
    print u"Cargando el índice..."
    buscador.cargar_indice()
    print u"Cargando las skip-lists..."
    buscador.cargar_skips()
    print u"Cargando el árbol B..."
    buscador.cargar_arbol_b()
    texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")
    while texto_consulta != "/q":
        parametros_consulta = tokenizar(texto_consulta)
        inicio_binario = time.time()
        respuesta = buscador.cargar_busqueda(parametros_consulta, buscador.MODO_BINARIO)
        fin_binario = time.time()
        print u"Resultados de la búsqueda"
        if respuesta:
            inicio_arbol = time.time()
            buscador.cargar_busqueda(parametros_consulta, buscador.MODO_ARBOL)
            fin_arbol = time.time()
            tiempo_binario = fin_binario - inicio_binario
            tiempo_arbol = fin_arbol - inicio_arbol
            print "ID_DOC"
            for id_doc in respuesta:
                print str(id_doc)
            print "----------------------------------------"
            print u"TIEMPO CONSULTA ARCHIVO BINARIO: " + str(tiempo_binario) + " segundos"
            print u"TIEMPO CONSULTA ÁRBOL B: " + str(tiempo_arbol) + " segundos"
        else:
            print u"No se obtuvo ningún resultado"
        texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "ERROR: Debe ingresar el directorio que contiene los indices."
        print "MODO DE USO: buscar_booleano_ej08.py <path_indices>"
    else:
        main(sys.argv[1])
