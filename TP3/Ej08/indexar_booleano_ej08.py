# -*- coding: utf-8 -*-

import sys
import codecs
from os import makedirs
from os.path import join, isdir
from struct import Struct


class ParseadorPostings(object):

    def __init__(self, path_postings):
        self.path_postings = path_postings
        self.vocabulario = {}

    def cargar_vocabulario(self):
        with codecs.open(self.path_postings, mode="r", encoding="utf-8") as file_postings:
            for linea in file_postings:
                termino, df_termino, ids_docs = linea.split(":")
                ids_docs = ids_docs.strip("\n").strip(",").split(",")
                ids_docs = map(int, ids_docs)
                self.vocabulario[termino] = sorted(ids_docs)


class Indexador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FILE_TERMINOS = "terminos.txt"
    FILE_DOCUMENTOS = "documentos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    FILE_SKIPS = "skips.bin"
    INTERVALO_SKIP = 5

    def __init__(self, vocabulario, dir_indices):
        self.crear_directorio(dir_indices)
        self.vocabulario = vocabulario
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_documentos = join(dir_indices, self.FILE_DOCUMENTOS)
        self.path_postings = join(dir_indices, self.FILE_POSTINGS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_skips = join(dir_indices, self.FILE_SKIPS)
        self.indice = {}
        self.terminos = sorted([termino for termino in self.vocabulario])
        self.skips = {}

    @staticmethod
    def crear_directorio(path_dir):
        try:
            makedirs(path_dir)
        except OSError:
            if not isdir(path_dir):
                raise

    def guardar_terminos(self):
        with codecs.open(self.path_terminos, mode="w", encoding="utf-8") as file_terminos:
            for termino in self.terminos:
                file_terminos.write(termino + "\n")

    def guardar_postings(self):
        packer = Struct(self.FORMATO_POSTING)
        pos_posting = 0
        with open(self.path_postings, mode="wb") as file_postings:
            for id_termino, termino in enumerate(self.terminos):
                self.skips[id_termino] = []
                self.indice[id_termino] = pos_posting
                df_termino = len(self.vocabulario[termino])
                file_postings.write(packer.pack(df_termino))
                pos_posting += packer.size
                id_doc_anterior = 0
                for pos_doc, id_doc in enumerate(sorted(self.vocabulario[termino])):
                    file_postings.write(packer.pack(id_doc - id_doc_anterior))
                    id_doc_anterior = id_doc
                    pos_posting += packer.size
                    if pos_doc % self.INTERVALO_SKIP == 0:
                        self.skips[id_termino].append((id_doc, pos_doc))

    def guardar_skips(self):
        packer = Struct(self.FORMATO_POSTING)
        with open(self.path_skips, mode="wb") as file_skips:
            for id_termino, termino in enumerate(self.terminos):
                file_skips.write(packer.pack(id_termino))
                file_skips.write(packer.pack(len(self.skips[id_termino])))
                for id_doc, pos_doc in self.skips[id_termino]:
                    file_skips.write(packer.pack(id_doc))
                    file_skips.write(packer.pack(pos_doc))

    def guardar_indice(self):
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="wb") as file_indice:
            for id_termino in sorted(self.indice):
                file_indice.write(packer.pack(id_termino, self.indice[id_termino]))


def main(dir_postings, dir_indices):
    print u"Parseando las posting-lists..."
    parseador = ParseadorPostings(dir_postings)
    parseador.cargar_vocabulario()
    print u"Parsing completo, vocabulario cargado..."
    indexador = Indexador(parseador.vocabulario, dir_indices)
    print u"Guardando los nombres de los términos..."
    indexador.guardar_terminos()
    print u"Guardando las posting-lists..."
    indexador.guardar_postings()
    print u"Guardando el indice..."
    indexador.guardar_indice()
    print u"Guardando las skips..."
    indexador.guardar_skips()
    print u"Finalizado!"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "ERROR: Debe ingresar el archivo que contiene las postings y el directorio destino de los indices."
        print "MODO DE USO: indexar_booleano_ej08.py <path_postings> <path_indices>"
    else:
        main(sys.argv[1], sys.argv[2])
