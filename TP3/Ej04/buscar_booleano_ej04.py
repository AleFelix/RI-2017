# -*- coding: utf-8 -*-

import sys
import codecs
from struct import Struct
from os.path import join
from indexar_booleano_ej04 import Tokenizador


class Buscador(object):
    FORMATO_POSTING = "I"
    FORMATO_POSICION = "H"
    FORMATO_INDICE = "I I"
    FILE_TERMINOS = "terminos.txt"
    FILE_DOCUMENTOS = "documentos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    AND = "and"
    OR = "or"
    NOT = "not"
    ADJ = "adj"
    CERCA = "cer"
    MAX_DISTANCIA = 5
    ERROR_CONSULTA =\
        u"ERROR: La consulta debe tener el formato '[<not>] <término> [(<and>|<or>|<and not>|<adj>|<cer>) <término>]'"

    def __init__(self, dir_indices):
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_documentos = join(dir_indices, self.FILE_DOCUMENTOS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_postings = join(dir_indices, self.FILE_POSTINGS)
        self.terminos = []
        self.documentos = []
        self.indice = {}

    def cargar_terminos(self):  # Archivo de texto con los nombres de los términos separados por saltos de línea
        with codecs.open(self.path_terminos, mode="r", encoding="utf-8") as file_terminos:
            for termino in file_terminos:
                self.terminos.append(termino.strip())

    def cargar_documentos(self):  # Archivo de texto con los nombres de los documentos separados por saltos de línea
        with codecs.open(self.path_documentos, mode="r", encoding="utf-8") as file_documentos:
            for documento in file_documentos:
                self.documentos.append(documento.strip())

    def cargar_indice(self):  # Formato del indice: [id_término, byte donde empieza la posting del término]
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="rb") as file_indice:
            bytes_indice = file_indice.read(packer.size)
            while bytes_indice:
                elementos_indice = packer.unpack(bytes_indice)
                self.indice[elementos_indice[0]] = elementos_indice[1]
                bytes_indice = file_indice.read(packer.size)

    def cargar_postings(self, lista_ids_terminos):  # Formato de las postings: df_term, id_doc, frec_term, pos_term
        packer_id = Struct(self.FORMATO_POSTING)
        packer_pos = Struct(self.FORMATO_POSICION)
        with open(self.path_postings, mode="rb") as file_postings:
            docs_por_termino = {}
            for id_termino in lista_ids_terminos:
                docs_por_termino[id_termino] = {}
                if id_termino in self.indice:
                    pos_posting = self.indice[id_termino]
                    file_postings.seek(pos_posting)
                    bytes_df = file_postings.read(packer_id.size)
                    df_termino = packer_id.unpack(bytes_df)[0]
                    for i in xrange(df_termino):
                        bytes_doc = file_postings.read(packer_id.size)
                        id_doc = packer_id.unpack(bytes_doc)[0]
                        docs_por_termino[id_termino][id_doc] = []
                        bytes_tf = file_postings.read(packer_id.size)
                        frec_term = packer_id.unpack(bytes_tf)[0]
                        for j in xrange(frec_term):
                            bytes_pos = file_postings.read(packer_pos.size)
                            pos_term = packer_pos.unpack(bytes_pos)[0]
                            docs_por_termino[id_termino][id_doc].append(pos_term)
            return docs_por_termino

    def obtener_ids_terminos(self, lista_terminos):
        lista_ids = []
        for termino in lista_terminos:
            if termino in self.terminos:
                lista_ids.append(self.terminos.index(termino))
        return lista_ids

    @staticmethod
    def unir(lista_1, lista_2):
        return sorted(list(set(lista_1) | set(lista_2)))

    @staticmethod
    def intersectar(lista_1, lista_2):
        return sorted(list(set(lista_1) & set(lista_2)))

    @staticmethod
    def restar(lista_1, lista_2):
        return sorted(list(set(lista_1) - set(lista_2)))

    def cercanos(self, posting_1, posting_2, distancia_max):
        docs_term_1 = posting_1.keys()
        docs_term_2 = posting_2.keys()
        interseccion = self.intersectar(docs_term_1, docs_term_2)
        lista_docs_con_adyacentes = []
        for id_doc in interseccion:
            for posicion in posting_1[id_doc]:
                for i in xrange(1, distancia_max + 1):
                    if posicion + i in posting_2[id_doc] or posicion - i in posting_2[id_doc]:
                        lista_docs_con_adyacentes.append(id_doc)
                        break
                if id_doc in lista_docs_con_adyacentes:
                    break
        return sorted(lista_docs_con_adyacentes)

    def cargar_busqueda(self, params):
        cant_params = len(params)
        operadores = [self.OR, self.AND, self.NOT, self.ADJ, self.CERCA]
        if cant_params == 0:
            print u"ERROR: Debe ingresar al menos un término de busqueda"
        elif cant_params == 1 and params[0] in operadores:
            print u"ERROR: La busqueda no puede contener únicamente un operador sin términos"
        elif cant_params == 2:
            print self.ERROR_CONSULTA
        elif cant_params == 3 and (params[0] in operadores or params[1] not in [self.OR, self.AND, self.ADJ, self.CERCA]
                                   or params[2] in operadores):
            print self.ERROR_CONSULTA
        elif cant_params == 4 and ((params[0] != self.NOT and params[1] != self.AND and
                                    params[2] not in [self.AND, self.NOT]) or
                                   (params[0] == self.NOT and params[2] == self.AND and params[1] in operadores) or
                                   (params[1] == self.AND and params[2] == self.NOT and params[0] in operadores) or
                                   (params[0] == self.NOT and params[2] != self.AND) or
                                   (params[0] != self.NOT and params[2] == self.AND) or
                                   (params[1] == self.AND and params[2] != self.NOT) or
                                   (params[1] != self.AND and params[2] == self.NOT) or
                                   (params[3] in operadores)):
            print self.ERROR_CONSULTA
        elif cant_params > 4:
            print u"ERROR: Solo puede ingresar hasta 4 parámetros"
        else:
            respuesta = self.buscar(params)
            print u"Resultados de la búsqueda"
            if respuesta:
                print "ID_DOC\t\tNOMBRE_DOC"
                for id_doc in respuesta:
                    print str(id_doc) + "\t\t\t" + self.documentos[id_doc]
            else:
                print u"No se obtuvo ningún resultado"

    def buscar(self, lista_parametros):
        terminos_query = []
        for parametro in lista_parametros:
            if parametro not in [self.OR, self.AND, self.NOT, self.ADJ, self.CERCA]:
                terminos_query.append(parametro)
        lista_ids = self.obtener_ids_terminos(terminos_query)
        if not lista_ids:
            return None
        else:
            postings = self.cargar_postings(lista_ids)
            posting_1 = postings[lista_ids[0]]
            posting_2 = postings[lista_ids[1]] if len(lista_ids) > 1 else {}
            cant_params = len(lista_parametros)
            if cant_params == 1:
                return sorted(posting_1.keys())
            elif cant_params == 3:
                if lista_parametros[1] == self.OR:
                    return self.unir(posting_1.keys(), posting_2.keys())
                elif lista_parametros[1] == self.AND:
                    return self.intersectar(posting_1.keys(), posting_2.keys())
                elif lista_parametros[1] == self.ADJ:
                    return self.cercanos(posting_1, posting_2, 1)
                else:
                    return self.cercanos(posting_1, posting_2, self.MAX_DISTANCIA)
            else:
                if lista_parametros[0] == self.NOT:
                    return self.restar(posting_2.keys(), posting_1.keys())
                else:
                    return self.restar(posting_1.keys(), posting_2.keys())


def main(dir_indices):
    buscador = Buscador(dir_indices)
    buscador.cargar_terminos()
    buscador.cargar_documentos()
    buscador.cargar_indice()
    texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")
    while texto_consulta != "/q":
        parametros_consulta = Tokenizador.tokenizar(texto_consulta)
        buscador.cargar_busqueda(parametros_consulta)
        texto_consulta = unicode(raw_input("Ingrese su consulta (/q para salir):"), "utf-8")


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "ERROR: Debe ingresar el directorio que contiene los indices."
        print "MODO DE USO: buscar_booleano_ej04.py <path_indices>"
    else:
        main(sys.argv[1])
