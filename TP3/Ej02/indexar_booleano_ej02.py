# -*- coding: utf-8 -*-

import re
import sys
import csv
import codecs
import numpy as np
from os import walk, makedirs
from os.path import join, getsize, isdir
from struct import Struct


class Tokenizador(object):
    MIN_LEN_TERM = 3
    MAX_LEN_TERM = 20

    def __init__(self, path_corpus, min_len=MIN_LEN_TERM, max_len=MAX_LEN_TERM, path_vacias=None):
        self.path_corpus = path_corpus
        self.sacar_vacias = False if path_vacias is None else True
        self.vacias = None
        self.min_len = min_len
        self.max_len = max_len
        self.vocabulario = {}
        self.id_doc = 0
        self.cantidad_docs = 0
        self.nombres_docs = []
        self.len_docs = []
        self.cant_tokens_doc_actual = None
        self.cant_terminos_doc_actual = None
        self.cant_terminos_unicos_doc_actual = None
        self.nombre_doc_actual = None
        self.doc_actual_tiene_terminos = None
        self.cargar_lista_vacias(path_vacias)

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    @staticmethod
    def tokenizar(texto):
        texto = texto.lower()
        texto = Tokenizador.translate(texto)
        texto = re.sub(u"[^a-zñ]|_", " ", texto)
        return texto.split()

    def cargar_lista_vacias(self, path_vacias):
        if self.sacar_vacias and path_vacias is not None:
            with codecs.open(path_vacias, mode="r", encoding="utf-8") as file_vacias:
                texto_vacias = file_vacias.read()
                self.vacias = self.tokenizar(texto_vacias)

    def analizar_corpus(self):
        for raiz, dirs, nombres_docs in walk(unicode(self.path_corpus)):
            for nombre_doc in nombres_docs:
                if (self.id_doc + 1) % 100 == 0:
                    sys.stdout.write("\r" + str(self.id_doc + 1) + " documentos procesados...")
                    sys.stdout.flush()
                self.nombre_doc_actual = nombre_doc
                self.doc_actual_tiene_terminos = False
                self.cant_tokens_doc_actual = 0
                self.cant_terminos_doc_actual = 0
                self.cant_terminos_unicos_doc_actual = 0
                path_doc = join(raiz, nombre_doc)
                self.analizar_documento(path_doc)
                if self.doc_actual_tiene_terminos:
                    self.len_docs.append({"tokens": self.cant_tokens_doc_actual,
                                          "terminos": self.cant_terminos_doc_actual,
                                          "terminos_unicos": self.cant_terminos_unicos_doc_actual})
                    self.id_doc += 1
        self.cantidad_docs = self.id_doc
        sys.stdout.write("\n")
        sys.stdout.flush()

    def analizar_documento(self, path_doc):
        with codecs.open(path_doc, mode="r", encoding="utf-8", errors="ignore") as file_doc:
            texto_doc = file_doc.read()
            tokens = self.tokenizar(texto_doc)
            if self.sacar_vacias:
                tokens = list(set(tokens) - set(self.vacias))
            self.cargar_vocabulario(tokens)

    def cargar_vocabulario(self, tokens):
        for token in tokens:
            self.cant_tokens_doc_actual += 1
            if self.min_len < len(token) < self.max_len:
                self.cant_terminos_doc_actual += 1
                if token not in self.vocabulario:
                    self.vocabulario[token] = {}
                if self.id_doc in self.vocabulario[token]:
                    self.vocabulario[token][self.id_doc] += 1
                else:
                    self.vocabulario[token][self.id_doc] = 1
                    self.cant_terminos_unicos_doc_actual += 1
                if not self.doc_actual_tiene_terminos:
                    self.nombres_docs.append(self.nombre_doc_actual)
                    self.doc_actual_tiene_terminos = True


class Indexador(object):
    FORMATO_POSTING = "I"
    FORMATO_INDICE = "I I"
    FILE_TERMINOS = "terminos.txt"
    FILE_DOCUMENTOS = "documentos.txt"
    FILE_POSTINGS = "postings.bin"
    FILE_INDICE = "indice.bin"
    FILE_ESTADISTICAS = "estadisticas.txt"
    FILE_CSV = "estadisticas.csv"
    FILE_CSV_TF = "estadisticas_tf.csv"

    def __init__(self, vocabulario, lista_docs, len_docs, dir_indices):
        self.crear_directorio(dir_indices)
        self.vocabulario = vocabulario
        self.path_terminos = join(dir_indices, self.FILE_TERMINOS)
        self.path_documentos = join(dir_indices, self.FILE_DOCUMENTOS)
        self.path_postings = join(dir_indices, self.FILE_POSTINGS)
        self.path_indice = join(dir_indices, self.FILE_INDICE)
        self.path_estadisticas = join(dir_indices, self.FILE_ESTADISTICAS)
        self.path_csv = join(dir_indices, self.FILE_CSV)
        self.path_csv_tf = join(dir_indices, self.FILE_CSV_TF)
        self.indice = {}
        self.terminos = sorted([termino for termino in self.vocabulario])
        self.docs = lista_docs
        self.len_postings = {}
        self.pesos = {}
        self.len_docs = len_docs

    @staticmethod
    def crear_directorio(path_dir):
        try:
            makedirs(path_dir)
        except OSError:
            if not isdir(path_dir):
                raise

    def guardar_terminos(self):
        with codecs.open(self.path_terminos, mode="w", encoding="utf-8") as file_terminos:
            for termino in self.terminos:
                file_terminos.write(termino + "\n")

    def guardar_nombres_docs(self):
        with codecs.open(self.path_documentos, mode="w", encoding="utf-8") as file_docs:
            for nombre_doc in self.docs:
                file_docs.write(nombre_doc + "\n")

    def calcular_estadisticas_postings(self):
        lista_len_postings = [len(self.vocabulario[termino]) for termino in self.terminos]
        self.len_postings["min"] = min(lista_len_postings)
        self.len_postings["max"] = max(lista_len_postings)
        self.len_postings["prom"] = round(sum(lista_len_postings) / float(len(self.terminos)), 2)
        self.len_postings["mediana"] = np.median(np.array(lista_len_postings))

    def calcular_estadisticas_overhead(self):
        self.pesos["docs"] = [d["terminos"] * 4 for d in self.len_docs]
        self.pesos["corpus"] = sum(peso_doc for peso_doc in self.pesos["docs"])
        peso_postings = getsize(self.path_postings)
        peso_indice = getsize(self.path_indice)
        self.pesos["estructura"] = peso_postings + peso_indice
        self.pesos["estructuras_docs"] = []
        self.pesos["estructuras_docs_tf"] = []
        self.pesos["overhead"] = []
        self.pesos["overhead_tf"] = []
        bytes_id_doc = 4
        bytes_id_doc_tf = 8
        for id_doc, doc in enumerate(self.docs):
            len_estructura_doc = self.len_docs[id_doc]["terminos_unicos"]
            peso_doc = self.pesos["docs"][id_doc]
            peso_estructura_doc = len_estructura_doc * bytes_id_doc
            peso_estructura_doc_tf = len_estructura_doc * bytes_id_doc_tf
            overhead = round(peso_estructura_doc / float(peso_estructura_doc + peso_doc), 4) * 100
            overhead_tf = round(peso_estructura_doc_tf / float(peso_estructura_doc_tf + peso_doc), 4) * 100
            self.pesos["estructuras_docs"].append(peso_estructura_doc)
            self.pesos["estructuras_docs_tf"].append(peso_estructura_doc_tf)
            self.pesos["overhead"].append(overhead)
            self.pesos["overhead_tf"].append(overhead_tf)

    def guardar_estadisticas(self):
        with codecs.open(self.path_estadisticas, mode="w", encoding="utf-8") as file_est:
            file_est.write(u"ESTADÍSTICAS DE LA INDEXACIÓN\n")
            file_est.write("-------------------------------------------------\n")
            file_est.write(u"LONGITUDES DE POSTINGS\n")
            file_est.write(u"MÍNIMA: " + str(self.len_postings["min"]) + " documentos\n")
            file_est.write(u"MÁXIMA: " + str(self.len_postings["max"]) + " documentos\n")
            file_est.write(u"PROMEDIO: " + str(self.len_postings["prom"]) + " documentos\n")
            file_est.write(u"MEDIANA: " + str(self.len_postings["mediana"]) + " documentos\n")
            file_est.write("-------------------------------------------------\n")
            file_est.write(u"OVERHEAD DE LA COLECCIÓN\n")
            file_est.write(u"TAMAÑO COLECCIÓN: " + str(self.pesos["corpus"]) + " bytes\n")
            file_est.write(u"TAMAÑO ESTRUCTURA: " + str(self.pesos["estructura"]) + " bytes\n")
            overhead = round(self.pesos["estructura"] / float(self.pesos["estructura"] + self.pesos["corpus"]), 4) * 100
            file_est.write(u"OVERHEAD TOTAL: " + str(overhead) + "%\n")
            file_est.write("-------------------------------------------------\n")
            file_est.write(u"OVERHEAD DE CADA DOCUMENTO\n")
            for id_doc, doc in enumerate(self.docs):
                peso_doc = self.pesos["docs"][id_doc]
                peso_estructura_doc = self.pesos["estructuras_docs"][id_doc]
                peso_estructura_doc_tf = self.pesos["estructuras_docs_tf"][id_doc]
                overhead = self.pesos["overhead"][id_doc]
                overhead_tf = self.pesos["overhead_tf"][id_doc]
                file_est.write("----\n")
                file_est.write(u"NOMBRE DOC: " + doc + "\n")
                file_est.write(u"TAMAÑO DOC: " + str(peso_doc) + " bytes\n")
                file_est.write(u"TAMAÑO ESTRUCTURA: " + str(peso_estructura_doc) + " bytes\n")
                file_est.write(u"TAMAÑO ESTRUCTURA CON TF: " + str(peso_estructura_doc_tf) + " bytes\n")
                file_est.write(u"OVERHEAD: " + str(overhead) + "%\n")
                file_est.write(u"OVERHEAD CON TF: " + str(overhead_tf) + "%\n")

    def guardar_estadisticas_csv(self):
        with open(self.path_csv, mode="wb") as file_csv:
            csv_writer = csv.writer(file_csv, delimiter=",")
            for id_doc, doc in enumerate(self.docs):
                cant_tokens = self.len_docs[id_doc]["tokens"]
                cant_terminos = self.len_docs[id_doc]["terminos"]
                overhead = self.pesos["overhead"][id_doc]
                csv_writer.writerow([id_doc, cant_tokens, cant_terminos, overhead])
        with open(self.path_csv_tf, mode="wb") as file_csv:
            csv_writer = csv.writer(file_csv, delimiter=",")
            for id_doc, doc in enumerate(self.docs):
                cant_tokens = self.len_docs[id_doc]["tokens"]
                cant_terminos = self.len_docs[id_doc]["terminos"]
                overhead_tf = self.pesos["overhead_tf"][id_doc]
                csv_writer.writerow([id_doc, cant_tokens, cant_terminos, overhead_tf])

    def guardar_postings(self):
        packer = Struct(self.FORMATO_POSTING)
        pos_posting = 0
        with open(self.path_postings, mode="wb") as file_postings:
            for id_termino, termino in enumerate(self.terminos):
                self.indice[id_termino] = pos_posting
                df_termino = len(self.vocabulario[termino])
                file_postings.write(packer.pack(df_termino))
                pos_posting += packer.size
                for id_doc in sorted(self.vocabulario[termino]):
                    file_postings.write(packer.pack(id_doc))
                    pos_posting += packer.size

    def guardar_indice(self):
        packer = Struct(self.FORMATO_INDICE)
        with open(self.path_indice, mode="wb") as file_indice:
            for id_termino in sorted(self.indice):
                file_indice.write(packer.pack(id_termino, self.indice[id_termino]))


def main(dir_corpus, dir_indices, path_vacias=None):
    tokenizador = Tokenizador(dir_corpus, path_vacias=path_vacias)
    print u"Extrayendo los términos de la colección..."
    tokenizador.analizar_corpus()
    print u"Extracción terminada, generando los indices..."
    indexador = Indexador(tokenizador.vocabulario, tokenizador.nombres_docs, tokenizador.len_docs, dir_indices)
    print u"Guardando los nombres de los términos..."
    indexador.guardar_terminos()
    print u"Guardando los nombres de los documentos..."
    indexador.guardar_nombres_docs()
    print u"Guardando las posting-lists..."
    indexador.guardar_postings()
    print u"Guardando el indice..."
    indexador.guardar_indice()
    print u"Calculando las estadisticas..."
    indexador.calcular_estadisticas_postings()
    indexador.calcular_estadisticas_overhead()
    print u"Guardando las estadisticas..."
    indexador.guardar_estadisticas()
    indexador.guardar_estadisticas_csv()
    print u"Finalizado!"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "ERROR: Debe ingresar el directorio que contiene el corpus y el directorio destino de los indices."
        print "MODO DE USO: indexar_booleano_ej02.py <path_corpus> <path_indices> [<path_vacias>]"
    else:
        if len(sys.argv) > 3:
            main(sys.argv[1], sys.argv[2], sys.argv[3])
        else:
            main(sys.argv[1], sys.argv[2])
