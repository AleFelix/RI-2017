# -*- coding: utf-8 -*-

REL = "R"
NO_REL = "N"

salida = [REL, NO_REL, NO_REL, REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, REL, NO_REL]
cant_total_relevantes = 11


def calcular_recall_precision(lista_respuestas, total_relevantes, posicion):
    cant_rel_respuesta = sum(1 for r in lista_respuestas[:posicion] if r == REL)
    tam_respuesta = len(lista_respuestas[:posicion])
    recall = cant_rel_respuesta / float(total_relevantes)
    precision = cant_rel_respuesta / float(tam_respuesta)
    return [round(recall, 2), round(precision, 2)]


# def precision_desde_recall(porcentaje_recall, total_relevantes, tam_respuesta):
#     recall = porcentaje_recall / 100.0
#     cant_relevantes_respuesta = total_relevantes * recall
#     return round(cant_relevantes_respuesta / float(tam_respuesta), 2)


def obtener_precision_r(lista_respuestas, lista_precisiones):
    pos_ultimo_relevante = max(pos_r for pos_r, res in enumerate(lista_respuestas) if res == REL)
    return lista_precisiones[pos_ultimo_relevante]


def calcular_precisiones_interpoladas(lista_recalls, lista_precisiones):
    puntos_recall = [x / 10.0 for x in xrange(11)]
    lista_precisiones_interpoladas = []
    for recall in puntos_recall:
        pos_r = next((i for i, r in enumerate(lista_recalls) if r >= recall), None)
        if pos_r is not None:
            precision = max(lista_precisiones[pos_r:])
        else:
            precision = 0.0
        lista_precisiones_interpoladas.append(precision)
    return [puntos_recall, lista_precisiones_interpoladas]

lista_total_recall = []
lista_total_precision = []

for pos in xrange(1, len(salida) + 1):
    rec, pre = calcular_recall_precision(salida, cant_total_relevantes, pos)
    lista_total_recall.append(rec)
    lista_total_precision.append(pre)

promedio_recall = round(sum(lista_total_recall) / float(len(lista_total_recall)), 2)
promedio_precision = round(sum(lista_total_precision) / float(len(lista_total_precision)), 2)
recalls_inter, precisiones_inter = calcular_precisiones_interpoladas(lista_total_recall, lista_total_precision)

print "RECALL Y PRECISION DE CADA POSICION"
print "{:5s}{:8s}{:10s}".format("Rel", "Recall", "Precision")
for indice, elemento in enumerate(salida):
    print "{:5s}{:8s}{:10s}".format(elemento, str(lista_total_recall[indice]), str(lista_total_precision[indice]))
print
print "RECALL Y PRECISION PROMEDIO"
print "{:8s}{:10s}".format("Recall", "Precision")
print "{:8s}{:10s}".format(str(promedio_recall), str(promedio_precision))
print
# print "PRECISION AL 50% de RECALL"
# print "Precision: " + str(precision_desde_recall(50, cant_total_relevantes, len(salida)))
# print
print "PRECISION-R"
print "Precision: " + str(obtener_precision_r(salida, lista_total_precision))
print
print "RECALL Y PRECISION INTERPOLADA"
print "{:8s}{:10s}".format("Recall", "Precision")
for indice, elemento in enumerate(recalls_inter):
    print "{:8s}{:10s}".format(str(recalls_inter[indice]), str(precisiones_inter[indice]))
