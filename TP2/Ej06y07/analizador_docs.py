# -*- coding: utf-8 -*-

import re
import sys
import codecs
from math import log
from os import listdir
from os.path import join
from math import sqrt

file_vacias = None
path_resultados = "ranking.txt"
file_queries = None


class Tokenizador(object):
    def __init__(self, path_corpus, path_vacias=None, min_len=0, max_len=100):
        self.min_len = min_len
        self.max_len = max_len
        self.vocabulario = {}
        self.path_corpus = path_corpus
        self.id_doc = 0
        self.cantidad_docs = 0
        self.documentos = {}
        self.vacias = None
        self.sacar_vacias = False
        if path_vacias is not None:
            self.sacar_vacias = True
            self.cargar_lista_vacias(path_vacias)

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    def cargar_lista_vacias(self, path_vacias):
        with codecs.open(path_vacias, mode="r", encoding="utf-8") as archivo_vacias:
            texto_vacias = archivo_vacias.read()
            self.vacias = self.tokenizar(texto_vacias)

    def analizar_corpus(self):
        for nombre_documento in listdir(self.path_corpus):
            self.documentos[self.id_doc] = nombre_documento
            path_documento = join(self.path_corpus, nombre_documento)
            self.analizar_documento(path_documento)
            self.id_doc += 1
        self.cantidad_docs = self.id_doc

    def analizar_documento(self, path_documento):
        with codecs.open(path_documento, mode="r", encoding="utf-8", errors="ignore") as documento:
            texto_documento = documento.read()
            tokens = self.tokenizar(texto_documento)
            if self.sacar_vacias:
                tokens = [token for token in tokens if token not in self.vacias]
            self.generar_terminos(tokens)

    @staticmethod
    def tokenizar(texto):
        texto = texto.lower()
        texto = Tokenizador.translate(texto)
        texto = re.sub(u"[^a-zñ]|_", " ", texto)
        return texto.split()

    def generar_terminos(self, tokens):
        for token in tokens:
            if self.min_len < len(token) < self.max_len:
                if token not in self.vocabulario:
                    self.vocabulario[token] = {}
                if self.id_doc in self.vocabulario[token]:
                    self.vocabulario[token][self.id_doc] += 1
                else:
                    self.vocabulario[token][self.id_doc] = 1

    @staticmethod
    def analizar_queries(path_queries):
        queries = []
        with codecs.open(path_queries, mode="r", encoding="utf-8") as archivo_queries:
            for linea in archivo_queries:
                if "<TITLE>" in linea:
                    query = re.sub("<TITLE>", "", linea)
                    terminos_query = Tokenizador.tokenizar(query)
                    queries.append(terminos_query)
        return queries


class Ponderador(object):
    def __init__(self, vocabulario, cant_docs):
        self.vocabulario = vocabulario
        self.cant_docs = cant_docs
        self.idf_terminos = {}
        self.pesos_terminos = {}
        self.max_tf_docs = {}

    def calcular_idfs(self):
        for termino in self.vocabulario:
            self.idf_terminos[termino] = log(float(self.cant_docs) / float(len(self.vocabulario[termino])))

    def calcular_max_tf_docs_vocabulario(self):
        for id_doc in xrange(self.cant_docs):
            max_frecuencia = 1
            for termino in self.vocabulario:
                if id_doc in self.vocabulario[termino]:
                    if self.vocabulario[termino][id_doc] > max_frecuencia:
                        max_frecuencia = self.vocabulario[termino][id_doc]
            self.max_tf_docs[id_doc] = max_frecuencia

    def calcular_pesos(self):
        self.calcular_max_tf_docs_vocabulario()
        for termino in self.vocabulario:
            self.pesos_terminos[termino] = {}
            for doc in self.vocabulario[termino]:
                self.pesos_terminos[termino][doc] = {}
                tf = self.vocabulario[termino][doc]
                idf = self.idf_terminos[termino]
                max_tf = self.max_tf_docs[doc]
                self.pesos_terminos[termino][doc] = (float(tf) / float(max_tf)) * float(idf)

    def ponderar_query(self, terminos_query):
        frecuencias_query = {}
        pesos_query = {}
        for termino in terminos_query:
            if termino in frecuencias_query:
                frecuencias_query[termino] += 1
            else:
                frecuencias_query[termino] = 1
        for termino in frecuencias_query:
            tf = frecuencias_query[termino]
            idf = 0
            if termino in self.idf_terminos:
                idf = self.idf_terminos[termino]
            pesos_query[termino] = {}
            max_tf = frecuencias_query[max(frecuencias_query)]
            pesos_query[termino] = (0.5 + (0.5 * float(tf)) / float(max_tf)) * float(idf)
        return pesos_query


class Comparador(object):

    @staticmethod
    def calcular_norma_vector(vector_pesos):
        return sqrt(sum([pow(float(peso), 2) for peso in vector_pesos]))

    @staticmethod
    def calcular_scores(pesos_terminos, pesos_query):
        lista_documentos = list(set([doc for term in pesos_query for doc in pesos_terminos[term]]))
        lista_terminos = [term for term in pesos_terminos]
        pesos_documentos = {}
        vector_pesos_query = []
        for documento in lista_documentos:
            pesos_documentos[documento] = []
            for termino in lista_terminos:
                if documento in pesos_terminos[termino]:
                    pesos_documentos[documento].append(pesos_terminos[termino][documento])
                else:
                    pesos_documentos[documento].append(0)
        for termino in lista_terminos:
            if termino in pesos_query:
                vector_pesos_query.append(pesos_query[termino])
            else:
                vector_pesos_query.append(0)
        return pesos_documentos, vector_pesos_query

    @staticmethod
    def calcular_similitud(pesos_documentos, vector_pesos_query):
        norma_query = Comparador.calcular_norma_vector(vector_pesos_query)
        sim_documentos = {}
        for documento in pesos_documentos:
            sim_documentos[documento] = {}
            vector_doc = pesos_documentos[documento]
            norma_documento = Comparador.calcular_norma_vector(vector_doc)
            sim_escalar = sum([float(p_doc) * float(p_query) for (p_doc, p_query) in zip(vector_doc, vector_pesos_query)])
            sim_coseno = float(sim_escalar) / float(float(norma_documento) * float(norma_query))
            sim_documentos[documento] = sim_coseno
        return sim_documentos

    @staticmethod
    def calcular_rankings(sim_documentos):
        lista_documentos = [doc for doc in sim_documentos]
        sim_coseno = [sim_documentos[doc] for doc in lista_documentos]
        ranking_coseno = sorted(zip(lista_documentos, sim_coseno), key=lambda x: x[1], reverse=True)
        return ranking_coseno


def recuperar_queries(path_corpus, path_queries, path_vacias, path_results):
    print u"\nRealizando indexación, por favor espere...\n"
    tokenizador = Tokenizador(path_corpus, path_vacias)
    tokenizador.analizar_corpus()
    ponderador = Ponderador(tokenizador.vocabulario, tokenizador.cantidad_docs)
    ponderador.calcular_idfs()
    ponderador.calcular_pesos()
    if path_queries is None:
        leer_queries_por_teclado(tokenizador, ponderador)
    else:
        procesar_archivo_queries(tokenizador, ponderador, path_queries, path_results)


def leer_queries_por_teclado(tokenizador, ponderador):
    print u"Indexación finalizada, ya puede realizar sus consultas\n"
    print "Para finalizar ingrese /q\n"
    texto_query = raw_input("Ingrese su consulta: ")
    while texto_query != "/q":
        print ("\nRealizando la consulta...")
        texto_query = unicode(texto_query, "utf-8")
        terminos_query = tokenizador.tokenizar(texto_query)
        pesos_query = ponderador.ponderar_query(terminos_query)
        pesos_docs, vector_query = Comparador.calcular_scores(ponderador.pesos_terminos, pesos_query)
        sim_docs = Comparador.calcular_similitud(pesos_docs, vector_query)
        ranking_coseno = Comparador.calcular_rankings(sim_docs)
        print "Consulta finalizada!\n"
        print "Ranking\n"
        print "ID_DOC\tSimilitud\tNombre_Doc\n"
        for rank in ranking_coseno:
            print str(rank[0]) + "\t" + "%0.10f" % rank[1] + "\t" + tokenizador.documentos[rank[0]] + "\n"
        print "\n\n"
        print "Para finalizar ingrese /q\n"
        texto_query = raw_input("Ingrese su consulta: ")
    print "Finalizado."


def procesar_archivo_queries(tokenizador, ponderador, path_queries, path_results):
    queries = tokenizador.analizar_queries(path_queries)
    with codecs.open(path_results, mode="w", encoding="utf-8") as file_ranks:
        print u"Indexación finalizada, generando rankings..."
        for id_query, query in enumerate(queries):
            print "Procesando consulta " + str(id_query + 1) + " de " + str(len(queries))
            pesos_query = ponderador.ponderar_query(query)
            file_ranks.write("Query: " + str(id_query) + "\n\n")
            pesos_docs, vector_query = Comparador.calcular_scores(ponderador.pesos_terminos, pesos_query)
            sim_docs = Comparador.calcular_similitud(pesos_docs, vector_query)
            ranking_coseno = Comparador.calcular_rankings(sim_docs)
            file_ranks.write("Ranking\n")
            file_ranks.write("ID_DOC\tSimilitud\tNombre_Doc\n")
            for rank in ranking_coseno:
                file_ranks.write(
                    str(rank[0]) + "\t" + "%0.10f" % rank[1] + "\t" + tokenizador.documentos[rank[0]] + "\n")
            file_ranks.write("\n")
        print "Finalizado!"


if __name__ == "__main__":
    if "-h" in sys.argv:
        print "MODO DE USO: analizador_docs.py <path_directorio_archivos> [-q <path_archivo_queries>] " \
              "[(-v <nombre_archivo_palabras_vacias>)]"
        sys.exit(0)
    if len(sys.argv) < 2:
        print "ERROR: Debe ingresar al menos el directorio con los archivos a analizar"
        sys.exit(1)
    if "-v" in sys.argv:
        if sys.argv.index("-v") + 1 == len(sys.argv):
            print "ERROR: Debe ingresar el nombre del archivo con palabras vacias"
            sys.exit(1)
        else:
            file_vacias = sys.argv[sys.argv.index("-v") + 1]
    if "-q" in sys.argv:
        if sys.argv.index("-q") + 1 == len(sys.argv):
            print "ERROR: Debe ingresar el path del archivo con las consultas"
            sys.exit(1)
        else:
            file_queries = sys.argv[sys.argv.index("-q") + 1]
    recuperar_queries(sys.argv[1], file_queries, file_vacias, path_resultados)
