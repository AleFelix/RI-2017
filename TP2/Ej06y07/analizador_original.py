# -*- coding: utf-8 -*-

import re
import sys
import codecs
from math import log
from os import listdir
from os.path import join
from math import sqrt
from math import e as euler

TF_IDF = "TF-IDF"
TF_NORM = "TF-NORM"
TF_IDF_NORM = "TF-IDF-NORM"
file_vacias = None
path_resultados = "ranking.txt"
# ESCALAR = "escalar"
# COSENO = "coseno"


class Tokenizador(object):
    def __init__(self, path_corpus, path_vacias=None, min_len=0, max_len=100):
        self.min_len = min_len
        self.max_len = max_len
        self.vocabulario = {}
        self.path_corpus = path_corpus
        self.id_doc = 0
        self.cantidad_docs = 0
        self.documentos = {}
        self.vacias = None
        self.sacar_vacias = False
        if path_vacias is not None:
            self.sacar_vacias = True
            self.cargar_lista_vacias(path_vacias)

    @staticmethod
    def translate(to_translate):
        tabin = u"áéíóú"
        tabout = u"aeiou"
        tabin = [ord(char) for char in tabin]
        translate_table = dict(zip(tabin, tabout))
        return to_translate.translate(translate_table)

    def cargar_lista_vacias(self, path_vacias):
        with codecs.open(path_vacias, mode="r", encoding="utf-8") as archivo_vacias:
            texto_vacias = archivo_vacias.read()
            self.vacias = self.tokenizar(texto_vacias)

    def analizar_corpus(self):
        for nombre_documento in listdir(self.path_corpus):
            self.documentos[nombre_documento] = self.id_doc
            path_documento = join(self.path_corpus, nombre_documento)
            self.analizar_documento(path_documento)
            self.id_doc += 1
        self.cantidad_docs = self.id_doc

    def analizar_documento(self, path_documento):
        with codecs.open(path_documento, mode="r", encoding="utf-8", errors="ignore") as documento:
            texto_documento = documento.read()
            tokens = self.tokenizar(texto_documento)
            if self.sacar_vacias:
                tokens = [token for token in tokens if token not in self.vacias]
            self.generar_terminos(tokens)

    @staticmethod
    def tokenizar(texto):
        texto = texto.lower()
        texto = Tokenizador.translate(texto)
        texto = re.sub(u"[^a-zñ]|_", " ", texto)
        return texto.split()

    def generar_terminos(self, tokens):
        for token in tokens:
            if self.min_len < len(token) < self.max_len:
                if token not in self.vocabulario:
                    self.vocabulario[token] = {}
                if self.id_doc in self.vocabulario[token]:
                    self.vocabulario[token][self.id_doc] += 1
                else:
                    self.vocabulario[token][self.id_doc] = 1

    @staticmethod
    def analizar_queries(path_queries):
        queries = []
        with codecs.open(path_queries, mode="r", encoding="utf-8") as archivo_queries:
            for linea in archivo_queries:
                if "<TITLE>" in linea:
                    query = re.sub("<TITLE>", "", linea)
                    terminos_query = Tokenizador.tokenizar(query)
                    queries.append(terminos_query)
        return queries


class Ponderador(object):
    def __init__(self, vocabulario, cant_docs):
        self.vocabulario = vocabulario
        self.cant_docs = cant_docs
        self.idf_terminos = {}
        self.tipos_pesos = [TF_IDF, TF_NORM, TF_IDF_NORM]
        self.pesos_terminos = {}

    def calcular_idfs(self):
        for termino in self.vocabulario:
            self.idf_terminos[termino] = log(self.cant_docs / float(len(self.vocabulario[termino])))

    def calcular_pesos(self):
        for termino in self.vocabulario:
            self.pesos_terminos[termino] = {}
            for doc in self.vocabulario[termino]:
                self.pesos_terminos[termino][doc] = {}
                tf = self.vocabulario[termino][doc]
                idf = self.idf_terminos[termino]
                for tipo in self.tipos_pesos:
                    if tipo == TF_IDF:
                        self.pesos_terminos[termino][doc][tipo] = tf * idf
                    if tipo == TF_NORM:
                        self.pesos_terminos[termino][doc][tipo] = 1 + log(tf)
                    if tipo == TF_IDF_NORM:
                        self.pesos_terminos[termino][doc][tipo] = (1 + log(tf)) * idf

    def ponderar_query(self, terminos_query):
        frecuencias_query = {}
        pesos_query = {}
        for termino in terminos_query:
            if termino in frecuencias_query:
                frecuencias_query[termino] += 1
            else:
                frecuencias_query[termino] = 1
        for termino in frecuencias_query:
            tf = frecuencias_query[termino]
            idf = 0
            if termino in self.idf_terminos:
                idf = self.idf_terminos[termino]
            pesos_query[termino] = {}
            for tipo in self.tipos_pesos:
                if tipo == TF_IDF:
                    max_tf = frecuencias_query[max(frecuencias_query)]
                    pesos_query[termino][tipo] = (0.5 + 0.5 * tf / float(max_tf)) * idf
                if tipo == TF_NORM:
                    pesos_query[termino][tipo] = log(1 + round(pow(euler, idf), 10))
                if tipo == TF_IDF_NORM:
                    pesos_query[termino][tipo] = (1 + log(tf)) * idf
        return pesos_query


class Comparador(object):

    @staticmethod
    def calcular_norma_vector(vector_pesos):
        return sqrt(sum([pow(peso, 2) for peso in vector_pesos]))

    @staticmethod
    def calcular_scores(pesos_terminos, pesos_query, tipo_peso):
        lista_documentos = list(set([doc for term in pesos_query for doc in pesos_terminos[term]]))
        lista_terminos = [term for term in pesos_terminos]
        pesos_documentos = {}
        vector_pesos_query = []
        for documento in lista_documentos:
            pesos_documentos[documento] = []
            for termino in lista_terminos:
                if documento in pesos_terminos[termino]:
                    pesos_documentos[documento].append(pesos_terminos[termino][documento][tipo_peso])
                else:
                    pesos_documentos[documento].append(0)
        for termino in lista_terminos:
            if termino in pesos_query:
                vector_pesos_query.append(pesos_query[termino][tipo_peso])
            else:
                vector_pesos_query.append(0)
        return pesos_documentos, vector_pesos_query

    @staticmethod
    def calcular_similitud(pesos_documentos, vector_pesos_query):
        norma_query = Comparador.calcular_norma_vector(vector_pesos_query)
        sim_documentos = {}
        for documento in pesos_documentos:
            sim_documentos[documento] = {}
            vector_doc = pesos_documentos[documento]
            norma_documento = Comparador.calcular_norma_vector(vector_doc)
            sim_escalar = sum([p_doc * p_query for (p_doc, p_query) in zip(vector_doc, vector_pesos_query)])
            sim_coseno = sim_escalar / float(norma_documento * norma_query)
            # sim_documentos[documento][ESCALAR] = sim_escalar
            # sim_documentos[documento][COSENO] = sim_coseno
            sim_documentos[documento] = sim_coseno
        return sim_documentos

    @staticmethod
    def calcular_rankings(sim_documentos):
        lista_documentos = [doc for doc in sim_documentos]
        # sim_escalar = [sim_documentos[doc][ESCALAR] for doc in lista_documentos]
        # sim_coseno = [sim_documentos[doc][COSENO] for doc in lista_documentos]
        sim_coseno = [sim_documentos[doc] for doc in lista_documentos]
        # ranking_escalar = sorted(zip(lista_documentos, sim_escalar), key=lambda x: x[1], reverse=True)
        ranking_coseno = sorted(zip(lista_documentos, sim_coseno), key=lambda x: x[1], reverse=True)
        # return ranking_escalar, ranking_coseno
        return ranking_coseno


def recuperar_queries(path_corpus, path_queries, path_vacias, path_results):
    tokenizador = Tokenizador(path_corpus, path_vacias)
    tokenizador.analizar_corpus()
    queries = tokenizador.analizar_queries(path_queries)
    ponderador = Ponderador(tokenizador.vocabulario, tokenizador.cantidad_docs)
    ponderador.calcular_idfs()
    ponderador.calcular_pesos()
    # for id_query, query in enumerate(queries):
    #     pesos_query = ponderador.ponderar_query(query)
    #     print "Query: " + str(id_query) + "\n"
    #     for tipo_peso in ponderador.tipos_pesos:
    #         pesos_docs, vector_query = Comparador.calcular_scores(ponderador.pesos_terminos, pesos_query, tipo_peso)
    #         sim_docs = Comparador.calcular_similitud(pesos_docs, vector_query)
    #         # ranking_escalar, ranking_coseno = Comparador.calcular_rankings(sim_docs)
    #         ranking_coseno = Comparador.calcular_rankings(sim_docs)
    #         print "Ranking\n"
    #         print "ID_DOC\tSimilitud"
    #         for rank in ranking_coseno:
    #             print str(rank[0]) + "\t" + str(rank[1])
    #         print
    with codecs.open(path_results, mode="w", encoding="utf-8") as file_ranks:
        print "Generando rankings..."
        for id_query, query in enumerate(queries):
            print "Procesando consulta " + str(id_query+1) + " de " + str(len(queries))
            pesos_query = ponderador.ponderar_query(query)
            file_ranks.write("Query: " + str(id_query) + "\n\n")
            for tipo_peso in ponderador.tipos_pesos:
                pesos_docs, vector_query = Comparador.calcular_scores(ponderador.pesos_terminos, pesos_query, tipo_peso)
                sim_docs = Comparador.calcular_similitud(pesos_docs, vector_query)
                ranking_coseno = Comparador.calcular_rankings(sim_docs)
                file_ranks.write("Ranking " + str(tipo_peso) + "\n")
                file_ranks.write("ID_DOC\tSimilitud\tNombre_Doc\n")
                for rank in ranking_coseno:
                    file_ranks.write(str(rank[0]) + "\t" + str(rank[1]) + tokenizador.documentos[rank[0]] + "\n")
                file_ranks.write("\n")
        print "Finalizado!"


if __name__ == "__main__":
    if "-h" in sys.argv:
        print u"MODO DE USO: analizador_docs.py <path_directorio_archivos> <path_archivo_queries> " \
              u"[(-v <nombre_archivo_palabras_vacias>)]"
        sys.exit(0)
    if len(sys.argv) < 3:
        print u"ERROR: Debe ingresar al menos el directorio con los archivos a analizar y el archivo de queries"
        sys.exit(1)
    if "-v" in sys.argv:
        if sys.argv.index("-v") + 1 == len(sys.argv):
            print u"ERROR: Debe ingresar el nombre del archivo con palabras vacias"
            sys.exit(1)
        else:
            file_vacias = sys.argv[sys.argv.index("-v") + 1]
    recuperar_queries(sys.argv[1], sys.argv[2], file_vacias, path_resultados)
