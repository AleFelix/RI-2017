# -*- coding: utf-8 -*-

import codecs
from os.path import split

path_nombres_docs = "resultado_terrier/collection.spec"
path_resultados = "resultado_terrier/TF_IDF_1.res"
path_procesado = "resultado_terrier/TF_IDF_1_procesado.res"


def leer_nombres(path_nombres):
    dic_nombres = {}
    with codecs.open(path_nombres, mode="r", encoding="utf-8") as archivo_nombres:
        for indice, linea in enumerate(archivo_nombres):
            if indice > 0:
                path, nombre = split(linea)
                dic_nombres[indice] = nombre
    return dic_nombres


def leer_resultados(path_results):
    lista_resultados = []
    with codecs.open(path_results, mode="r", encoding="utf-8") as archivo_resultados:
        for linea in archivo_resultados:
            items = linea.split()
            lista_resultados.append([items[0], items[2], items[4]])
    return lista_resultados


def guardar_archivo_procesado(path_proc, dic_nombres, lista_resultados):
    with codecs.open(path_proc, mode="w", encoding="utf-8") as archivo_resultados:
        archivo_resultados.write("ID_Q\tID_DOC\tTF_IDF\t\tNOMBRE_DOC\n")
        for resultado in lista_resultados:
            archivo_resultados.write(resultado[0] + "\t" + resultado[1] + "\t" + "%0.10f" % float(resultado[2]) + "\t"
                                     + dic_nombres[int(resultado[1])])

if __name__ == "__main__":
    dict_nombres = leer_nombres(path_nombres_docs)
    lista_res = leer_resultados(path_resultados)
    guardar_archivo_procesado(path_procesado, dict_nombres, lista_res)
