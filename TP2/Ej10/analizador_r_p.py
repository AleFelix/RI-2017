# -*- coding: utf-8 -*-

REL = "R"
NO_REL = "N"

Q1_SA = [REL, REL, REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL,
         NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL]

Q1_SB = [NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL,
         NO_REL, NO_REL, NO_REL, REL, REL, NO_REL, NO_REL, NO_REL, REL, NO_REL]

Q1_SC = [NO_REL, REL, NO_REL, NO_REL, REL, REL, REL, NO_REL, NO_REL, REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL,
         NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL]

Q2_SA = [REL, NO_REL, REL, REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, REL, NO_REL,
         NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL]

Q2_SB = [NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL,
         NO_REL, NO_REL, REL, NO_REL, NO_REL, REL, NO_REL, REL, REL, NO_REL]

Q2_SC = [NO_REL, REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL,
         NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, REL, NO_REL]

Q3_SA = [NO_REL, REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL,
         REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, REL]

Q3_SB = [NO_REL, REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, REL, NO_REL, NO_REL,
         REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL]

Q3_SC = [NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, REL, NO_REL, NO_REL, REL, REL, REL, REL,
         NO_REL, NO_REL, REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL, NO_REL]

lista_totales_relevantes = [6, 7, 7]

lista_queries = [[Q1_SA, Q1_SB, Q1_SC], [Q2_SA, Q2_SB, Q2_SC], [Q3_SA, Q3_SB, Q3_SC]]

nombre_sistemas = ["A", "B", "C"]

dict_sistemas = {}

lista_posiciones_precision = [5, 10, 20]


def calcular_recall_precision(lista_respuestas, total_relevantes, posicion):
    cant_rel_respuesta = sum(1 for r in lista_respuestas[:posicion] if r == REL)
    tam_respuesta = len(lista_respuestas[:posicion])
    recall = cant_rel_respuesta / float(total_relevantes)
    precision = cant_rel_respuesta / float(tam_respuesta)
    return [round(recall, 2), round(precision, 2)]


def calcular_precisiones_interpoladas(lista_recalls, lista_precisiones):
    puntos_recall = [x / 10.0 for x in xrange(11)]
    lista_precisiones_interpoladas = []
    for recall in puntos_recall:
        pos_r = next((i for i, r in enumerate(lista_recalls) if r >= recall), None)
        if pos_r is not None:
            precision = max(lista_precisiones[pos_r:])
        else:
            precision = 0.0
        lista_precisiones_interpoladas.append(precision)
    return [puntos_recall, lista_precisiones_interpoladas]


def calcular_recall_precision_respuesta(respuesta, cant_total_relevantes):
    lista_total_recall = []
    lista_total_precision = []
    for pos in xrange(1, len(respuesta) + 1):
        rec, pre = calcular_recall_precision(respuesta, cant_total_relevantes, pos)
        lista_total_recall.append(rec)
        lista_total_precision.append(pre)
    promedio_precision = round(sum(lista_total_precision) / float(len(lista_total_precision)), 2)
    recalls_inter, precisiones_inter = calcular_precisiones_interpoladas(lista_total_recall, lista_total_precision)
    return lista_total_precision, promedio_precision, precisiones_inter


for nombre in nombre_sistemas:
    dict_sistemas[nombre] = []

for index, query in enumerate(lista_queries):
    for idx, sistema in enumerate(query):
        dict_sistemas[nombre_sistemas[idx]].append(calcular_recall_precision_respuesta(sistema,
                                                                                       lista_totales_relevantes[index]))

for id_nombre, sistema in enumerate(dict_sistemas):
    promedio_posiciones = [0] * len(dict_sistemas[sistema][0][0])
    promedio_precision = 0
    promedio_interpoladas = [0] * len(dict_sistemas[sistema][0][2])
    for resultado in dict_sistemas[sistema]:
        promedio_precision += resultado[1]
        for pos, precision in enumerate(resultado[0]):
            promedio_posiciones[pos] += precision
        for pos, precision in enumerate(resultado[2]):
            promedio_interpoladas[pos] += precision
    cant_querys = len(dict_sistemas[sistema])
    promedio_precision = promedio_precision / float(cant_querys)
    for pos, precision in enumerate(resultado[0]):
        promedio_posiciones[pos] = promedio_posiciones[pos] / float(cant_querys)
    for pos, precision in enumerate(resultado[2]):
        promedio_interpoladas[pos] = promedio_interpoladas[pos] / float(cant_querys)
    print "SISTEMA " + nombre_sistemas[id_nombre] + "\n"
    print u"Precisión promedio: " + str(promedio_precision) + "\n"
    for idx_porcentaje in xrange(0, 11, 2):
        print u"Precisión al " + str(idx_porcentaje * 10) + "% de recall: " + str(round(
            promedio_interpoladas[idx_porcentaje], 2))
    print
    for pos in lista_posiciones_precision:
        print u"Promedio P@" + str(pos) + ": " + str(round(promedio_posiciones[pos-1], 2))
    print
