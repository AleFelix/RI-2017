# -*- coding: utf-8 -*-

import re
from sys import stdout as out
import codecs

file_vocabulary = "data/vocabulary.txt"
file_queries = "data/queries.txt"
file_doc_vector = "data/documentVector.txt"
file_rel_docs = "data/relevantDocs"


class AnalizadorQueries(object):
    def __init__(self, path_voc, path_queries, path_doc_vector, path_rel_docs):
        self.vocabulario = {}
        self.queries = {}
        self.vectores_docs = {}
        self.doc_relevantes = {}
        self.leer_vocabulario(path_voc)
        self.leer_queries(path_queries)
        self.leer_vectores_docs(path_doc_vector)
        self.leer_doc_relevantes(path_rel_docs)

    def leer_vocabulario(self, path_voc):
        with codecs.open(path_voc, mode="r", encoding="utf-8") as archivo_vocabulario:
            for indice, linea in enumerate(archivo_vocabulario):
                elementos_termino = linea.split()
                if indice > 0 and len(elementos_termino) == 3:
                    self.vocabulario[elementos_termino[0]] = elementos_termino[1]

    def leer_queries(self, path_queries):
        with codecs.open(path_queries, mode="r", encoding="utf-8") as archivo_queries:
            for indice, linea in enumerate(archivo_queries):
                terminos_query = re.search("\(.*\)", linea)
                if terminos_query:
                    terminos = terminos_query.group()
                    terminos = re.sub("[(),]", " ", terminos)
                    lista_terminos = terminos.split()
                    self.queries[str(indice)] = lista_terminos

    def leer_vectores_docs(self, path_doc_vector):
        with codecs.open(path_doc_vector, mode="r", encoding="utf-8") as archivo_doc_vector:
            for indice, linea in enumerate(archivo_doc_vector):
                terminos_documento = re.search("\(.*\)", linea)
                if terminos_documento:
                    terminos = terminos_documento.group()
                    terminos = re.sub("[(),]", " ", terminos)
                    lista_terminos = terminos.split()
                    self.vectores_docs[str(indice)] = lista_terminos

    def leer_doc_relevantes(self, path_rel_docs):
        with codecs.open(path_rel_docs, mode="r", encoding="utf-8") as archivo_rel_docs:
            for indice, linea in enumerate(archivo_rel_docs):
                documentos_relevantes = re.search("\(.*\)", linea)
                if documentos_relevantes:
                    documentos = documentos_relevantes.group()
                    documentos = re.sub("[(),]", " ", documentos)
                    lista_documentos = documentos.split()
                    self.doc_relevantes[str(indice)] = lista_documentos

    def procesar_queries(self):
        for query in sorted(self.queries, key=int):
            terminos_query = self.queries[query]
            idf_terminos_query = []
            lista_resultados = []
            lista_intersecciones = []
            lista_scores = []
            lista_scores_totales = []
            for termino in terminos_query:
                idf_terminos_query.append(self.vocabulario[termino])
            for documento in sorted(self.vectores_docs, key=int):
                terminos_doc = self.vectores_docs[documento]
                terminos_interseccion = [termino for termino in terminos_query if termino in terminos_doc]
                if terminos_interseccion:
                    lista_resultados.append(documento)
                    lista_intersecciones.append(terminos_interseccion)
            for terminos_interseccion in lista_intersecciones:
                score_terminos = []
                for termino in terminos_interseccion:
                    score_terminos.append(self.vocabulario[termino])
                lista_scores.append(score_terminos)
            for score_terminos in lista_scores:
                score_total = 0
                for termino in score_terminos:
                    score_total += float(termino)
                lista_scores_totales.append(round(score_total,2))
            indice_orden_scores = sorted(range(len(lista_scores_totales)), key=lambda x: lista_scores_totales[x],
                                         reverse=True)
            ranking_resultados = [lista_resultados[i] for i in indice_orden_scores]
            doc_relevantes = self.doc_relevantes[query]
            cant_doc_rel_query = len([doc for doc in doc_relevantes if doc in lista_resultados])
            print "Query " + query + ": (" + ", ".join(terminos_query) + ")"
            print "IDF Terminos: (" + ", ".join(idf_terminos_query) + ")"
            print "Resultado modelo booleano: (" + ", ".join(lista_resultados) + ")"
            print "Score de documentos encontrados:"
            for indice, interseccion in enumerate(lista_intersecciones):
                out.write("\t-Doc " + str(lista_resultados[indice]) + ": (" + ", ".join(interseccion) + ") = " +
                          " + ".join(lista_scores[indice]))
                if len(lista_scores[indice]) > 1:
                    out.write(" = " + str(lista_scores_totales[indice]))
                out.write("\n")
                out.flush()
            print "Ranking modelo vectorial: (" + ", ".join(ranking_resultados) + ")"
            print "Relevantes: (" + ", ".join(doc_relevantes) + ")"
            print "Precision: " + str(cant_doc_rel_query) + "/" + str(len(lista_resultados)) + " = " +\
                  str(round(cant_doc_rel_query / float(len(lista_resultados)), 2))
            print "Recall: " + str(cant_doc_rel_query) + "/" + str(len(doc_relevantes)) + " = " +\
                  str(round(cant_doc_rel_query / float(len(doc_relevantes)), 2))
            print

if __name__ == "__main__":
    analizador_queries = AnalizadorQueries(file_vocabulary, file_queries, file_doc_vector, file_rel_docs)
    analizador_queries.procesar_queries()
