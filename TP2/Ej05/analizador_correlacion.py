# -*- coding: utf-8 -*-

import codecs


def main():
    archivo_tf_idf = "TF_IDF_1.res"
    archivo_bm25 = "BM25b0.75_0.res"
    archivo_resultados = "Resultados.txt"
    cant_queries = 5
    max_respuestas = 50
    respuestas_tf_idf = []
    respuestas_bm25 = []
    limites_sublistas = [10, 25, 50]
    resultados_por_limite = []

    cargar_lista_respuestas(archivo_tf_idf, respuestas_tf_idf, cant_queries, max_respuestas)
    cargar_lista_respuestas(archivo_bm25, respuestas_bm25, cant_queries, max_respuestas)

    for limite in limites_sublistas:
        resultados_sublistas = []
        print "Listas con " + str(limite) + " resultados:"
        for indice_query in xrange(cant_queries):
            print "Query N°" + str(indice_query + 1)
            resultado = analizar_sub_listas(respuestas_tf_idf[indice_query], respuestas_bm25[indice_query], limite)
            resultado.append(indice_query + 1)
            resultados_sublistas.append(list(resultado))
        resultados_por_limite.append(list(resultados_sublistas))
        print

    guardar_resultados(archivo_resultados, resultados_por_limite, limites_sublistas)


def cargar_lista_respuestas(nombre_archivo, lista_respuestas, cant_queries, max_respuestas):
    with codecs.open(nombre_archivo, mode="r", encoding="utf-8") as archivo:
        id_query = 1
        contador = 0
        ranking_documentos = []
        for linea in archivo:
            respuesta = linea.split()
            if respuesta[0] == str(id_query):
                ranking_documentos.append(respuesta[2])
                contador += 1
            if contador >= max_respuestas:
                lista_respuestas.append(list(ranking_documentos))
                id_query += 1
                contador = 0
                ranking_documentos = []
            if id_query > cant_queries:
                break


def normalizar_listas(lista_1, lista_2):
    for documento in lista_1:
        if documento not in lista_2:
            lista_2.append(documento)


def analizar_sub_listas(respuestas_tf_idf, respuestas_bm25, limite):
    sub_respuestas_tf_idf = respuestas_tf_idf[:limite]
    sub_respuestas_bm25 = respuestas_bm25[:limite]
    normalizar_listas(sub_respuestas_tf_idf, sub_respuestas_bm25)
    normalizar_listas(sub_respuestas_bm25, sub_respuestas_tf_idf)
    n = len(sub_respuestas_tf_idf)
    total_di = 0
    for documento in sub_respuestas_tf_idf:
        indice_1 = sub_respuestas_tf_idf.index(documento) + 1
        indice_2 = sub_respuestas_bm25.index(documento) + 1
        total_di += pow(abs(indice_1 - indice_2), 2)
    coeficiente = calcular_coeficiente_corr(total_di, n)
    print "Coef. Corr: " + str(coeficiente)
    return [sub_respuestas_tf_idf, sub_respuestas_bm25, coeficiente]


def calcular_coeficiente_corr(total_di, n):
    divisor = n * (pow(n, 2) - 1)
    return 1 - ((6 * total_di) / float(divisor))


def guardar_resultados(nombre_archivo, lista_resultados_por_limite, limites):
    with codecs.open(nombre_archivo, mode="w", encoding="utf-8") as archivo:
        for indice_limite, limite in enumerate(limites):
            archivo.write("Listas con " + str(limite) + " resultados:\n")
            for resultados in lista_resultados_por_limite[indice_limite]:
                archivo.write(u"Query N°" + str(resultados[3]) + "\n")
                archivo.write("Rank\tTF-IDF\tBM25\n")
                for pos in xrange(len(resultados[0])):
                    archivo.write(str(pos + 1) + "\t" + resultados[0][pos] + "\t" + resultados[1][pos] + "\n")
                archivo.write("Coef. Corr: " + str(resultados[2]) + "\n\n")

if __name__ == "__main__":
    main()
