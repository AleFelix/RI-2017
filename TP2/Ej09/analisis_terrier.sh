#!/bin/bash

echo "Directorio de analisis: $1"
echo "Directorio de consultas: $2"
echo "Directorio de juicios de relevancia: $3"

read -p "Presione una tecla para continuar..."

echo "Borrando los indices y resultados previos"

rm -r ./terrier-4.0/var/index/*
rm -r ./terrier-4.0/var/results/*

read -p "Hecho, presione una tecla para continuar..."

./terrier-4.0/bin/trec_setup.sh $1

read -p "Presione una tecla para continuar..."

./terrier-4.0/bin/trec_terrier.sh -i

read -p "Presione una tecla para continuar..."

./terrier-4.0/bin/trec_terrier.sh -r -Dtrec.model=TF_IDF -Dignore.low.idf.terms=false -Dtrec.topics=$2

read -p "Presione una tecla para continuar..."

./terrier-4.0/bin/trec_terrier.sh -e -Dtrec.qrels=$3